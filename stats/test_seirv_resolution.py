#!/usr/bin/env python3

##### INCLUDES

from emcee_shared import *

import gzip
import json
from multiprocessing import Pool, current_process
import numpy as np
import os
import pickle
import time
import sys


##### METHODS

def filename( combo ):
    kind, idx, res, pop, duration = combo
    return f'generated/seirv_res/seirv_res.{res}.{duration}.{kind}.{idx}.pickle.gz'

def evaluate( combo ):

    output = { 'pos':[], 'ml':[] }

    # figure out what our parameters are
    kind, idx, res, pop, duration = combo

    # skip files that already exist
    if os.path.isfile( filename(combo) ):
         return None

    # out of curiousity, how long does this take?
    startTime = time.time()

    # clean up the series
    data = clean_series( dataset[kind][idx][0], dataset[kind][idx][1] )
    # deltas can't be done with seirv

    # we're evaluating all time frames!
    seirv  = SEIR_Variable(pop, indep=np.array(data[0])/86400, depen=data[1], res=res)
    n    = len(seirv._indep)

    seed,_ = seirv.seed()
    lnl    = [ seirv.lnprob(theta) for theta in seed ]
    output['ml'].append( [max(lnl)/n,-1,-1,-1] )
    print( f"SE {current_process().pid:8d}: {res:2d}/{duration: 7.1f}@{len(output['ml']):3d} = {max(lnl)/n}", flush=True )

    # ensure we do at least two rounds
    maxsearch = 180
    if 2*maxsearch > duration:
        maxsearch = duration / 2
    bail = False

    # loop for a fixed amount of time, assume it's enough
    beginTime = time.time()
    nowTime   = time.time()
    while (nowTime - beginTime) < duration:

        # ensure we don't go over our limit
        remaining = duration - nowTime + beginTime
        if remaining < maxsearch:
            maxsearch = max(int(remaining) + 1, 0)
            bail = True

        # begin with MCMC
        pos, stats = seirv.run_sampler( seed=seed, maxTime=maxsearch, stepsize=4, \
                updateDelay=np.inf )
        lnl        = [ seirv.lnprob(theta) for theta in pos ]
        output['ml'].append( [max(lnl)/n,*stats] )
        print( f"MC {current_process().pid:8d}: {res:2d}/{duration: 7.1f}@{len(output['ml']):3d} = {max(lnl)/n}", flush=True )

        # bust out if out of time
        if bail:
            seed = pos
            break

        # ensure we don't go over our limit
        nowTime   = time.time()
        remaining = duration - nowTime + beginTime
        if remaining < maxsearch:
            maxsearch = max(int(remaining) + 1, 0)

        # narrow on ML via minimum
        maxlike,count = seirv.run_maxlike( guess=pos[np.argmax(lnl)], \
                maxTime=maxsearch, updateDelay=np.inf )
        if maxlike is not None:
            max_lnl = seirv.lnprob(maxlike)/n
            output['ml'].append( [max_lnl,count,-1,-1] )
            seed,_ = seirv.seed( guess=maxlike, wiggle=[1e-4]*len(maxlike) )
            print( f"ML {current_process().pid:8d}: {res:2d}/{duration: 7.1f}@{len(output['ml']):3d} = {max_lnl}", flush=True )
        else:
            seed = pos

        nowTime   = time.time()

    # generate a legit posterior
    steps = 2048 // len(seed)
    pos, stats = seirv.run_sampler( seed=pos, maxIters=steps, \
            maxTime=np.inf, updateDelay=np.inf, stepsize=4, burn=False )
    lnl        = [ seirv.lnprob(theta) for theta in pos ]

    output['pos'] = pos[-512:].tolist()
    output['ml'].append( [max(lnl)/n,*stats] )
    print( f"MC {current_process().pid:8d}: {res:2d}/{duration: 7.1f}@{len(output['ml']):3d} = {max(lnl)/n}", flush=True )
    print( f"FN {current_process().pid:8d}: {time.time() - startTime:.2f}s", flush=True )

    return (output, combo)

##### MAIN

# read off the index/step from the command line
try:
    offset = int(sys.argv[1])
    step   = int(sys.argv[2])
except:
    print("ERROR: Please supply an offset and step size on the command line.")
    sys.exit(1)

# read in the dataset
with gzip.open( 'fitting_dataset.json.gz', 'rt', encoding="utf-8") as zipfile:
    dataset = json.load( zipfile )

# build up a to-do list
kind = 3        # simplify things by focusing just on New York City
idx = 1863
pop = 8336817

todo = list()
for res in range(3,36):
    for duration in [3600/(np.sqrt(2)**i) for i in range(11)]:
            todo.append( (kind, idx, res, pop, duration) )

# scramble it, deterministically
np.random.seed( 42 )
np.random.shuffle( todo )
np.random.seed()

# fire up multiprocessing
with Pool() as pool:

    for result in pool.imap_unordered( evaluate, todo[offset::step], 1 ):
#    for result in map( evaluate, todo[offset::step] ):

        if result is None:                                        # skip invalid results
            continue

        output, combo = result
        if (output is None) or (len(output) < 1):           # skip invalid results
            continue

        # gzip support, courtesy https://stackoverflow.com/questions/49534901/is-there-a-way-to-use-json-dump-with-gzip/49535758#49535758
        with gzip.open( filename( combo ), 'wb', compresslevel=9 ) as zfile:
            pickle.dump( {'index':combo,'out':output}, zfile )
        print( combo )
        sys.stdout.flush()
