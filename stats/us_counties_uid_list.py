#!/usr/bin/python3

import numpy as np
import pandas as pd

us_counties = pd.read_csv('https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_deaths_US.csv')
us_c_sorted = us_counties.sort_values( '6/30/20' )
states = pd.Categorical( us_counties['Province_State'] ).categories
valid = (us_c_sorted['6/30/20'] > 0) & (us_c_sorted['Population'] > 0)

uids = set()
for idx,state in enumerate(states):
    subset = us_c_sorted[ (us_c_sorted['Province_State'] == state) & valid ]
    if len(subset) < 4:
        for _,row in subset.iterrows():
            uids.add( row['UID'] )
    else:
        uids.add( subset.iloc[ 0]['UID'] )
        uids.add( subset.iloc[-1]['UID'] )
        midpoint = (1 + len(subset)) * .5
        if (int(midpoint) != midpoint) and (idx & 1):
                midpoint = int(midpoint) + 1
        else:
                midpoint = int(midpoint)
        uids.add( subset.iloc[midpoint]['UID'] )

np.savetxt( 'us_counties_uid_list.tsv', sorted(uids), fmt='%d' )

rows = list()
for row_i,row_v in us_counties.iterrows():
    if row_v['UID'] in uids:
        rows.append( row_i )

np.savetxt( 'us_counties_idx_list.tsv', sorted(rows), fmt='%d' )
