#!/usr/bin/env python3

##### INCLUDES

import gzip
import json
from multiprocessing import Pool, Manager
import numpy as np
import os
import pandas as pd
import pickle


##### VARIABLES

posterior_sample = 128		# how many values to sample from the posterior


##### METHODS

def evaluate( file ):

    # only bother with posterior datasets
    if not file.startswith("quality.1.0."):
        return None
    
    # load the projections up
    with gzip.open(f'trendlines/{file}', 'rt') as zfile:
        data = json.load( zfile )

    kind, idx = data['index']

    # also load up the posterior
    with gzip.open(f'trendlines/posteriors.1.0.x.{kind}.{idx}.json.gz', 'rt') as zfile:
        fits = json.load( zfile )

    # we want this separated by the length of line
    day_contents = [ [] for x in range(len(data['qual'])) ]
        
    for start_idx,start in enumerate(data['qual']):
        for idx in np.random.choice(len(data['qual'][start_idx]), posterior_sample):
                for i,day in enumerate(data['qual'][start_idx][idx]):
                
                    if i >= len(day_contents):
                        # print( f"DEBUG: overflow for i={i}, idx={start_idx}, file={file}" )
                        day_contents.append( [] )
                
                    # pull the associated fit
                    row = [day, fits['pos'][start_idx][2], *fits['pos'][start_idx][3][idx]]

                    day_contents[i].append( row )

    del fits
    return (data['index'], day_contents)

##### MAIN

dataset = list()

todo = os.listdir("trendlines")   # the files to consider
np.random.shuffle( todo )		# scramble them to better load-balance

# fire up multiprocessing
with Pool(10) as pool:

     for result in pool.imap_unordered( evaluate, todo, 1 ):
     
         if result is None:
             continue

         meta, day_contents, day_totals = result
         for i,array in enumerate(day_contents):
             for v in array:
                dataset.append( [meta[0], meta[1], i + 1, *v] )

         del meta
         del day_totals
     
pred = pd.DataFrame( dataset, columns=['type','series','day','mean(log(like))','timestamp','b','ln_mu','sigma','loc','log(σ)'])
del dataset
with gzip.open(f'loggauss_prediction_table.pickle.gz', 'wb', compresslevel=9) as zipfile:
     pickle.dump( pred, zipfile )

