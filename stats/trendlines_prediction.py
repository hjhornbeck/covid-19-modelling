#!/usr/bin/env python3

##### INCLUDES

from emcee_shared import *

import argparse
import gc
import gzip
import json
from multiprocessing import Pool, Process
import numpy as np
import os
import pickle
import time
import sys


##### METHODS

# use a function to construct the filename
def filename(file):
    return file.replace("posteriors","quality")

def write( storage, filename, verbose=False ):
    with gzip.open( filename, 'wb', compresslevel=9 ) as zfile:
        pickle.dump( storage, zfile )
    if verbose:
        print( f"= Wrote {filename}", flush=True )

# do the lowest level of processing
def inner_evaluate( data ):

    quality = list()

    # figure out what our parameters are
    days, index = data['index']
    kind, idx = index

    # load up the posterior and parameters
    fits   = data['pos']
    params = data['params']

    # clean up the series
    subset = clean_series( dataset[kind][idx][0], dataset[kind][idx][params['dataset']] )
    subset[1] = np.diff( [subset[1][0]] + list(subset[1]) )

    # for each saved fit
    for fit in fits:

        quality.append( [] )

        # determine the datapoints in the future
        i = 0
        while (subset[0][i] < fit[2]) and (i < len(subset[0])):
            i += 1

        # evaluate those future datapoints, if possible
        if i == len(subset[0]):
            continue

        indep = np.array([(x-subset[0][i-1])/86400 for x in subset[0][i:]])
        depen = subset[1][i:]
        tr_model = Trendline( depen=depen, indep=indep, minlnstd=params['minlnstd'], \
                clip=params['clip'], fade=params['fade'], prior=params['type'], \
                dist=params['dist'] )

        for theta in fit[3]:

            # build up the log probabilities
            lp = tr_model.lnprior( theta )
            if not np.isfinite( lp ):
                quality[-1].append( [] )    # gotta stay in sync
                continue

            llike = tr_model.lnlike( theta, consolidate=False )
            if type(llike) is float:
                if llike == -np.inf:
                    quality[-1].append( [] )    # gotta stay in sync
                else:
                    quality[-1].append( [llike] )
                continue

            # convert to an average and save
            else:
                quality[-1].append(list( np.cumsum(llike) / np.arange(1,len(llike)+1) ))

    # all done
    return {'index':data['index'], 'params':data['params'], 'qual':quality}

# allow file reads/writes to be multithreaded
def outer_evaluate( infile, outfile, threads, verbose ):

    with gzip.open( f"{infile}", 'rb' ) as zfile:
        bundle = pickle.load( zfile )

    # fire up multiprocessing
    with Pool(processes=threads, maxtasksperchild=8) as pool:

        # branch depending on the number of threads
        if threads == 1:
            results = map( inner_evaluate, bundle )
        else:
            results = pool.map( inner_evaluate, bundle, 3 )

    output = list()
    for data in results:

        # print out a status message, if requested
        if verbose:
                    days, index = data['index']
                    kind, idx = index
                    print( f"> {meta['names'][kind][idx]} ({kind}/{idx}@{days})", end='' )

                    membytes = sys.getallocatedblocks() << 12
                    if membytes < (1 << 30):
                        print( f" [{membytes/(1<<20):.1f}MB", end='')
                    else:
                        print( f" [{membytes/(1<<30):.1f}GB", end='')

                    print( f" {gc.get_count()}]", flush=True )

        # update the outputs and queue length
        output.append( data )

    # write out the bundle
    write( output, f"{outfile}", verbose )

    # encourage the garbage collector
    del output
    del results

##### MAIN

# get the garbage collector to be more aggressive
# gc.set_threshold( 100, 10, 10 )


parser = argparse.ArgumentParser( description="Assess the quality of the given fits")
parser.add_argument( '-v',"--verbose", action='store_true', help="Give more verbose update messages" )

parser.add_argument( '-d','--dataset', metavar="DATASET", default='fitting_with_meta.json.gz', \
                help="The datasets to use for fitting" )
parser.add_argument( 'indir', metavar="INDIR", default='generated/posteriors/0', \
                help="The input directory containing fitting files" )
parser.add_argument( 'outdir', metavar="OUTDIR", default='generated/quality/0', \
                help="The output directory for the fitting quality files" )

parser.add_argument( '-T',"--threads", type=int, default=0, help="The number of threads to use. Default = one per core." )
parser.add_argument( '-f',"--files", type=int, default=18, help="The number of files to process simultaneously. Tune to available memory." )
parser.add_argument( '-o',"--offset", type=int, default=0, help="The offset within the number of steps." )
parser.add_argument( '-s',"--stepsize", type=int, default=1, help="The number of steps to take." )
args = parser.parse_args()


# tweak the thread count
if args.threads <= 0:
    args.threads = len(os.sched_getaffinity(0))


# read in the datasets
with gzip.open( args.dataset, 'rt', encoding="utf-8") as zfile:
    meta = json.load( zfile )
dataset = meta['data']

# build up a to-do list
todo      = os.listdir( args.indir )
queue     = list()

# scramble it, deterministically
np.random.seed( 12 )
np.random.shuffle( todo )
subset = todo[args.offset::args.stepsize]

# while we have things to process
while (len(subset) > 0) or (len(queue) > 0):

    # fill up the queue if there's input/space
    while (len(subset) > 0) and (len(queue) < args.files):

            file = subset.pop(0)

            # skip this bundle if already processed
            if os.path.isfile( f"{args.outdir}/{filename(file)}" ):
                if args.verbose:
                    print( f"= Skipped {file}", flush=True )
                continue

            print( f"= Adding {args.indir}/{file} to the queue.", flush=True )

            # append outer_evaluate to the queue, if multiple files are allowed
            if args.files > 1:
                temp =  Process( target=outer_evaluate, \
                    args=(f"{args.indir}/{file}",f"{args.outdir}/{filename(file)}", args.threads, args.verbose) )
                temp.start()
                queue.append( temp )
            # otherwise, just process it on the spot
            else:
                outer_evaluate( f"{args.indir}/{file}",f"{args.outdir}/{filename(file)}", args.threads, args.verbose )

    # check for idle processes, eliminating them if found
    for idx,v in enumerate(queue):
        if not v.is_alive():
           queue.pop(idx)

print( "= Finished.", flush=True )
