#!/usr/bin/env python3

##### INCLUDES

from emcee_shared import *

import argparse
import gc
import gzip
import json
from multiprocessing import Pool
import numpy as np
import os
import pickle
import sys


##### VARIABLES


##### METHODS

# use a function to construct the filename
def filename(args,count):

    retVal = f"{args.outdir}/posteriors."
    if type(args.preset) is int:
        retVal += f"{args.preset}"
    else:
        retVal += f"{args.clip}.{args.fade}.{args.type}.{args.minlnstd}.{args.dist}"

    retVal += f".{args.dataset}.{args.stepsize}.{args.merge}.{args.offset}.{count}.pickle.gz"
    return retVal

def write( storage, filename, verbose=False ):
    with gzip.open( filename, 'wb', compresslevel=9 ) as zfile:
        pickle.dump( storage, zfile )
    if verbose:
        print( f"= Wrote {filename}", flush=True )

def evaluate( combo ):
    global dataset, rstate, args

    posterior = list()

    # figure out what our parameters are
    days, index = combo
    kind, idx = index

    # clean up the series
    series = 1
    if (args.dataset > 0) and (args.dataset < 6):
        series = args.dataset
    data = clean_series( dataset[kind][idx][0], dataset[kind][idx][series], \
            increasing=(args.dist == 'gamma'), start=args.startUTCsec )
    data_sc = np.array( data[0] ) / 86400
    data[1] = np.diff( [data[1][0]] + list(data[1]) )

    # start evaluating each time frame
    if (args.dist == 'gamma') and (data[1][0] == 0):
        left = 1    # skip the first entry, as it's zero
        right = 1
    else:
        left = 0
        right = 0

    while right < len(data[0]):

        # skip ahead until we've satisfied our span
        if (data_sc[right] - data_sc[left]) < days:
            right += 1
            continue


        # no point in testing too-small datasets
        indep  = data_sc[left:right]
        if len(indep) < 2:
            left += 1
            continue

        # continue setting up the data
        indep -= indep[-1]
        depen  = data[1][left:right]

        # also no point in fitting to flat zeros
        if sum(depen) == 0:
            left += 1
            continue

        # build and run the model
        tr_model   = Trendline( indep=indep, depen=depen, clip=args.clip, \
                fade=args.fade, prior=args.type, minlnstd=args.minlnstd, dist=args.dist )
        pos, stats = tr_model.run_sampler( maxTime=15, updateDelay=np.inf, stepsize=512, \
                rstate=rstate )

        count, bounces, stalls, lrstate = stats

        steps = args.posteriorsize // len(pos)
        pos, stats = tr_model.run_sampler( seed=pos, maxIters=steps, updateDelay=np.inf, \
                burn=False, stepsize=steps, rstate=lrstate )


        # start building the output
        temp = [right-left, data[0][left], data[0][right-1], [], []]
        for idx in np.random.choice( len(pos), size=args.posteriorsample ):

            theta = pos[idx]
            temp[3].append( list(theta) )
            temp[4].append( tr_model.lnprob(theta) )

        posterior.append( temp )

        # finally, advance a step
        left += 1

    try:        # there's a small chance these were never created
        del tr_model, indep, depen
    except UnboundLocalError:
        pass
    return (posterior, combo)

##### MAIN

# make the garbage collector more aggressive
# gc.set_threshold( 100, 10, 10 )


parser = argparse.ArgumentParser( description="Generate all possible exponential trendline fits to the dataset.")
parser.add_argument( '-v',"--verbose", action='store_true', help="Give more verbose update messages." )

parser.add_argument( '-p',"--preset", type=int, help="The preset to use. Ranges between 0 and 23, inclusive." )
parser.add_argument( '-c',"--clip", type=float, help="Manually supply a clip value." )
parser.add_argument( '-f',"--fade", type=float, help="Manually supply a fade value." )
parser.add_argument( '-t',"--type", help="Manually supply a base type." )
parser.add_argument( '-d',"--dataset", type=int, default=1, help="The dataset to use. 1 = cases, 2 = deaths." )
parser.add_argument( '-r',"--minlnstd", type=float, help="Manually supply a minimal log standard deviation." )
parser.add_argument( '-D',"--dist", default='gauss', help="The likelihood distribution to use: 'gauss', 'gamma', 'cauchy', 'student'." )
parser.add_argument( "--minwindow", type=int, default=3, help="The smallest window size to consider." )
parser.add_argument( "--maxwindow", type=int, default=28, help="The largest window size to consider." )
parser.add_argument( "--posteriorsize", type=int, default=2048, help="The size of the original posterior." )
parser.add_argument( "--posteriorsample", type=int, default=256, help="The size of the sample to take from the posterior." )
parser.add_argument( "--startUTCsec", type=int, help="The earliest datapoint to consider in the dataset, in seconds" + \
        " since the epoch UTC." )

parser.add_argument( '--seed', metavar="PICKLE", help="A Pickle'd random state." )
parser.add_argument( '-i','--input', metavar="DATASET", default='fitting_with_meta.json.gz', \
        help="The datasets to use for fitting." )
parser.add_argument( '-w','--whitelist', metavar="TSV", help="A whitelist of which datasets to include." )
parser.add_argument( 'outdir', metavar="OUTDIR", default='generated/posteriors/0', \
        help="The output directory for the fitting files." )

parser.add_argument( '-m','--merge', type=int, default=64, help="How many output files to bundle into one" )
parser.add_argument( '-T',"--threads", type=int, default=0, help="The number of threads to use. Default = one per core" )
parser.add_argument( '-o',"--offset", type=int, default=0, help="The offset within the number of steps" )
parser.add_argument( '-s',"--stepsize", type=int, default=1, help="The number of steps to take" )
args = parser.parse_args()


# tweak the thread count
if args.threads <= 0:
    args.threads = len(os.sched_getaffinity(0))

# handle the presets
if type(args.preset) is int:

    choices = ['gauss', 'gamma', 'cauchy', 'student']

    args.minlnstd = -1.3862943611198906  # fixed when using presets
    
    # fix the preset's range
    args.preset %= 6*len(choices)

    # which distribution are we using?
    dist = args.preset // 6
    if (dist < len(choices)) and (dist >= 0):
        args.dist = choices[dist]

    # figure out the rest
    temp = args.preset % 6
    if temp > 2:
        base_preset = temp - 3
        args.type   = 'flat'
    else:
        base_preset = temp
        args.type   = 'invtan'

    if base_preset == 0:     # every limit on
        args.clip = np.log(2)
        args.fade = args.clip * .5
    elif base_preset == 1:   # ditch the fade
        args.clip = np.log(2)
        args.fade = np.inf
    else:                    # no slope constraints
        args.clip = np.inf
        args.fade = np.inf


# if some values are missing, apply some defaults
if args.clip is None:
        args.clip     = np.log(2)
if args.fade is None:
        args.fade     = np.log(2) * .5
if args.type is None:
        args.type     = 'invtan'
if args.minlnstd is None:
        args.minlnstd = -1.3862943611198906

if args.verbose:
    print( f"= clip/fade/type/dist = {args.clip}/{args.fade}/{args.type}/{args.dist}." )


# read in the dataset
with gzip.open( args.input, 'rt', encoding="utf-8") as zipfile:
    meta = json.load( zipfile )
dataset = meta['data']

# read in the seed, if present
if args.seed:
    rstate = pickle.load(open( args.seed, 'rb' ))
    if args.verbose:
        print( f"= fixed random seed = {rstate[0]}/{rstate[1][0]}/{rstate[1][1]}/etc..",\
                flush=True ) # it could have been a while
else:
    rstate = None


# build up a to-do list
data_todo = list()

if args.whitelist:
    for line in np.loadtxt( args.whitelist, delimiter='\t', dtype=int ):
        data_todo.append( tuple(line) )
        
else:
    for kind_idx,kind in enumerate(dataset):
        for i,_ in enumerate(kind):
            data_todo.append( (kind_idx,i) )

todo = [(d, index) for d in range(args.minwindow,args.maxwindow) for index in data_todo]

if args.verbose:
    print( f"= Number of sims to run = {len(todo)}." )

# scramble it deterministically
np.random.seed( 42 )
np.random.shuffle( todo )

if args.verbose:
    print( f"= offset/stepsize = {args.offset}/{args.stepsize}." )

# fire up multiprocessing (kill off old workers to help free up stale resources)
with Pool(processes=args.threads, maxtasksperchild=4) as pool:

    count = 0
    storage = list()
    params = { 'clip':args.clip, 'fade':args.fade, 'type':args.type, \
            'dataset':args.dataset, 'minlnstd':args.minlnstd, 'dist':args.dist }
    if type(args.preset) is int:
        params['preset'] = args.preset

    # fast-forward past already-finished fits
    while os.path.isfile( filename(args,count) ):
        todo = todo[args.stepsize*args.merge:]
        count += 1

    if args.verbose:
        print( f"= Fast-forwarded past {count} previous bundles.", flush=True )

    # build the appropriate generator
    if args.threads == 1:
        gen = map( evaluate, todo[args.offset::args.stepsize] )
    else:
        gen = pool.imap( evaluate, todo[args.offset::args.stepsize], 2 )

    # pull results from our computations
    for result in gen:

        if result is None:                                        # skip invalid results
            continue

        posterior, combo = result
        if (posterior is None) or (len(posterior) < 1):           # skip more invalid results
            continue

        days, index = combo
        kind, idx = index

        storage.append( {'index':combo,'params':params,'pos':posterior} )
        if len(storage) >= args.merge:
            write( storage, filename(args,count), args.verbose )
            storage.clear()
            count += 1 

        if args.verbose:
            print( f"> {meta['names'][kind][idx]} ({kind}/{idx}@{days})", end='' )

            membytes = sys.getallocatedblocks() << 12
            if membytes < (1 << 30):
                print( f" [{membytes/(1<<20):.1f}MB", end='')
            else:
                print( f" [{membytes/(1<<30):.1f}GB", end='')

            print( f" {gc.get_count()}]", flush=True )


    if len(storage) > 0:
            write( storage, filename(args,count), args.verbose )

print( "= Finished.", flush=True )
