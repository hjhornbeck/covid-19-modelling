#!/usr/bin/env python3

##### INCLUDES

import emcee

import numpy as np

from scipy.integrate import solve_ivp
from scipy.interpolate import CubicSpline
from scipy.optimize import minimize as spop_min
import scipy.stats as spst
import sys

import time

##### VARIABLES


##### CLASSES

class EmceeModel:
    """The parent class for all MCMC models that use the emcee framework.
    Either override model()/model_lnprior() or lnprior()/lnlike() in subclasses."""

    @staticmethod
    def logistic( array ):
        """Convert the value or array from the range [-inf,inf] to [0,1]."""
        if (type(array) == tuple) or (type(array) == list):
            return 1. / (1. + np.exp(-np.array(array)))

        return 1. / (1. + np.exp(-array))

    @staticmethod
    def logit( array ):
        """Convert the value or array from the range [0,1] to [-inf,inf]. Can return NaNs!"""
        if (type(array) == tuple) or (type(array) == list):
            return -np.log( 1/np.array(array) - 1 )

        return -np.log( 1/array - 1 )

    @staticmethod
    def reparam_gamma( mean, stdev ):
        """Convert a mean and standard deviation into the gamma distribution's shape and scale."""
        return ( (mean/stdev)**2, (stdev**2)/mean )

    @staticmethod
    def gamma_logpdf( y, mu, stdev ):
        """A simple reparametarization of scipy.stats.gamma."""
        shape, scale = EmceeModel.reparam_gamma( mu, stdev )
        return spst.gamma.logpdf( y, shape, scale=scale )
        
    
    def __init__(self, indep=list(), depen=list(), guess=list(), wiggle=list(), minlnstd=None, dist='gauss'):
        """Initialize ourselves.
        indep = An iterable consisting of the independent data, finite in length. Doesn't have to be
           one-dimensional. Technically optional, practically required.
        depen = An interable consisting of the dependent data, finite in length. The default 
           routines assume this is one-dimensional. Technically optional, practically required.
        guess = A guess at the maximal likelihood of this model, without looking at the data.
        wiggle = The standard deviation of values to draw around the maximal likelihood.
        minlnstd = The minimal log standard deviation allowed. If blank, arbitrarily high precision
           is allowed.
        dist = The probability distribution used by lnlike() to evaluate model(). Choices are 'gauss'ian,
           'cauchy', 'student', and 'gamma', with the first being the default. Beware that 'gamma' can't 
           handle negative or zero values! The default degrees of freedom for student's t is 5, but
           this can be overridden by specifying 'student_X', where X is a positive integer greater than
           1."""

        self._indep    = indep
        self._depen    = depen
        self._guess    = guess + [0]
        self._wiggle   = wiggle + [1]
        self._minlnstd = minlnstd

        if dist == 'gamma':
            self._dist = EmceeModel.gamma_logpdf
        elif dist == 'cauchy':
            self._dist = lambda y, mu, std: spst.cauchy.logpdf( y, loc=mu, scale=std )
        elif dist.startswith('student'):
            try:
                df = int( dist[8:] )
                if df < 2:              # only certain values are permitted
                    df = 5
            except ValueError:
                df = 5
            self._dist = lambda y, mu, std: spst.t.logpdf( y, df, loc=mu, scale=std )
        else:
            self._dist = lambda y, mu, std: spst.norm.logpdf( y, loc=mu, scale=std )

        self._sampler = None        # the sampler is hidden away internally

    def model_lnprior(self, theta):
        """The prior for the given model parameters.
        Returns the log likelihood."""

        return -np.inf

    def model(self, theta, indep=list()):
        """For the given parameters and independent params, output the dependent values.
        If data is not supplied, use the dataset supplied with the constructor.
        Returns the predicted output."""

        return list()


    def lnprior(self, theta):
        """Calculate the prior probability of the given parameter set.
        Returns the log probability."""

        # this could be re-written to remove the discontinuity, but that takes
        #  more processing power and confuses the meaning of theta
        if (type(self._minlnstd) == float) and (theta[-1] < self._minlnstd):
            return -np.inf

        # use Jeffrey's prior for the variance.
        return self.model_lnprior(theta[:-1]) - theta[-1]  + 0.34657359027997264 # (.5*np.log(2))

    def lnlike(self, theta, indep=list(), depen=list(), consolidate=True):
        """Calculate the likelihood of the given parameter set.
        indep = the independent data. If left blank, the independent data passed to the
           constructor is used.
        depen = the predicted data. If left blank, the data passed to the
           constructor is used.
        consolidate = If True, combine all likelihoods together; if False, leave
           separate. Handy for evaluating predictions.
        Returns the log probability."""

        if (len(depen) < 1):
            indep = self._indep
            depen = self._depen
        elif len(indep) != len(depen):
            indep = self._indep
        assert len(indep) == len(depen)

        yp = self.model(theta[:-1], indep)
        if len(yp) != len(indep):           # these must be the same length
            return -np.inf
        if not np.isfinite(yp).all():
            return -np.inf                  # must be finite!

        retVal = self._dist( depen, yp, np.exp(theta[-1]) )
        if not (np.isfinite( retVal )).all():
            return -np.inf                  # filter out non-finite values

        if consolidate:
            return np.sum(retVal)
        else:
            return retVal


    def lnprob(self, theta, indep=list(), depen=list()):
        """A convenience function for lnlike() + lnprior(). Always consolidates.
        Returns the log probability."""

        # do these one-by-one, to minimize processing cost
        lpr = self.lnprior(theta)
        if not np.isfinite( lpr ):
            return -np.inf

        # also handy if either lnprior() or lnlike() can return NaNs
        llh = self.lnlike(theta, indep=indep, depen=depen)
        if not np.isfinite( llh ):
            return -np.inf

        return lpr + llh


    def print(self, theta):
        """Return the given parameter set as a string."""
        return ""

    def seed(self, n=None, guess=list(), wiggle=list(), indep=list(), depen=list(), rstate=None):
        """Generate a seed value for this model.
        n = the number of seed values to create. If None, check the sampler or return 128
        guess = an optional override to the default guess iterable.
        wiggle = an optional override to the default wiggle iterable.
        indep, depen = Some optional data to test against. Can help weed out invalid parameters.
        rstate = If present, seed numpy's RandomState RNG routines with this value.
        Returns (seed, trials), where seed is a list of parameter seeds and trials is
           the number of trials necessary to generate those seeds."""

        # if no guess/wiggle provided, go with the default
        if len(guess) < 1:
            guess = self._guess

        if len(wiggle) < 1:
            wiggle = self._wiggle

        if (len(guess) < 1) or (len(wiggle) < 1):
            return None

        assert len(guess) == len(wiggle)

        # revert to the built-in data if none given
        if (len(depen) < 1):
            indep = self._indep
            depen = self._depen
        elif len(indep) != len(depen):
            indep = self._indep
        assert len(indep) == len(depen)

        # how many items are we returning?
        if n is None:

            if self._sampler is None:
                n = len(guess) * 2
            else:
                n = self._sampler.nwalkers

        seed = list()
        trials = 0
        gen = np.random.RandomState()
        if rstate is not None:
            gen.set_state( rstate )

        while len(seed) < n:

            test = guess + wiggle*gen.randn( len(guess) )
            trials += 1

            lnpr = self.lnprior(test)
            if not np.isfinite( lnpr ):
                continue

            if len(depen) > 0:

                lnll = self.lnlike(test, indep=indep, depen=depen)
                if not np.isfinite( lnll ):
                    continue

            seed.append( test )

        return seed, trials

    def build_sampler(self, nwalkers=None, indep=list(), depen=list(), pool=None ):
        """Generate an emcee sampler for this model.
        nwalkers = the number of MCMC walkers to use. Default = len(guess) * 2
        indep = a iterable of the independent data to use. If None, use the one supplied in the constructor.
           If supplied and valid, replaces the internal value.
        depen = a iterable of the dependent data to use. If None, use the one supplied in the constructor.
           If supplied and valid, replaces the internal value.
        pool = a multiprocessing pool to draw workers from, to spread the work across multiple CPU cores.
        Returns True on success."""

        if (len(indep) > 0) and (len(indep) == len(depen)):
            self._indep = indep
            self._depen = depen
        elif len(depen) == len(self._indep):
            self._depen = depen

        if nwalkers is None:
            nwalkers = len( self._guess ) * 2

        self._sampler = emcee.EnsembleSampler(nwalkers, len(self._guess), self.lnprob, pool=pool)
        return True

    def run_sampler( self, seed=None, maxTime=120, maxIters=np.inf, updateDelay=5, stepsize=1, \
            burn=True, n=None, rstate=None ):
        """Execute a round of MCMC sampling.
        seed = the starting seed for the sampler. If not provided, it will be generated.
        maxTime = the maximal number of seconds to sample for.
        maxIters = the maximum number of iterations to sample for.
        updateDelay = the number of seconds between status updates.
        stepsize = the number of sampling steps to take at once. 
           Larger is more efficient but slows status updates.
        burn = a boolean value indicating whether or not these samples are to be discarded.
        n = the number of datapoints. Optional, used only for status updates.
        rstate = An initial value for numpy's RandomState RNG. Useful for replication.
        Returns the posterior, the number of iterations, how many times the ML decreased, 
           and how often the ML didn't change."""

        # fill in the sampler, if none exists
        if (self._sampler is None) and (not self.build_sampler()):
            return None, (None, None, None, None)       # bail if we can't generate one

        # tidy up or reject the parameters
        if (maxTime <= 0) or (maxIters <= 0):
            return None, (None, None, None, None)

        try:
            stepsize = int(stepsize)
        except:
            stepsize = 1
        if stepsize <= 0:
            stepsize = 1

        # fill in the seed, if none provided
        if seed is None:
            seed,_ = self.seed( rstate=rstate )

        if not burn:
            self._sampler.reset()

        # set up our variables
        pos    = seed
        lp     = None

        # while these checks aren't necessary, the docs state that if rstate is
        #  not in the correct form, the seeding attempt will silently fail.
        #  That's not ideal if we care about replication, so instead we try to make
        #  failures non-silent. Note that this rejects using a dict to seed emcee.
        if rstate is not None:
            assert len(rstate) == 5
            assert rstate[0] == "MT19937"
            assert type(rstate[1]) == np.ndarray
            assert len(rstate[1]) == 624
            assert rstate[1].dtype == np.dtype('uint32')
            assert type(rstate[2]) == int
            assert type(rstate[3]) == int
            assert type(rstate[4]) == float

            self._sampler.random_state = rstate

        lastTime         = time.time()
        beginTime        = time.time()

        lastML  = -np.inf
        bounces = 0
        stalls  = 0
        count   = 0
        l_count = -1
        if n is None:
            n       = len(self._depen)

        # both time limits and sample counts must be respected
        while ((time.time() - beginTime) < maxTime) and (count < maxIters):

            pos, lp, rstate = self._sampler.run_mcmc(pos, stepsize, log_prob0=lp, rstate0=rstate, \
                store=(not burn), skip_initial_state_check=True)

            count += stepsize

            idx = np.argmax(lp)
            if lastML > lp[idx]:
                bounces += 1
            elif lastML == lp[idx]:
                stalls += 1
            lastML = lp[idx]

            nowTime = time.time()
            if (nowTime - lastTime) > updateDelay:

                print( '{} {:5}: '.format('BURN' if burn else 'POST', count), end='' )
                print( self.print( pos[idx] ), end='' )
                if n > 0:
                    print( ' ({:.2f}/n)'.format(lp[idx]/n) )
                else:
                    print( ' ({:.2f})'.format(lp[idx]) )

                lastTime = nowTime
                l_count = count
                sys.stdout.flush()

        if np.isfinite( updateDelay ) and (l_count < count):
            print( '{} {:5}: '.format('BURN' if burn else 'POST', count), end='' )
            print( self.print( pos[idx] ), end='' )
            if n > 0:
                print( ' ({:.2f}/n)'.format(lp[idx]/n) )
            else:
                print( ' ({:.2f})'.format(lp[idx]) )

        if burn:
            return pos, (count, bounces, stalls, rstate)
        else:
            return self._sampler.get_chain(flat=True), (count, bounces, stalls, rstate)

    def run_maxlike( self, guess=None, maxTime=300, maxIters=np.inf, updateDelay=5, n=None ):
        """Try to iteratively find the maximal likelihood. Unlike run_sampler(), this is 
           a deterministic algorithm.
        guess = Starting guess for the algorithm. Will use the stored guess if not given.
        maxTime = How long to search for, in seconds.
        maxIters = The maximum number of iterations to perform.
        updateDelay = How many seconds to wait between status updates.
        n = The number of datapoints. Optional, only used for the status display.
        Returns the best maximal likelihood found."""

        # no guess? Go with the built-in
        if (guess is None) or (len(guess) != len(self._guess)):
            guess = self._guess

        # tidy up or reject the parameters
        if (maxTime <= 0) or (maxIters <= 0):
            return None, -1

        lastTime         = time.time()
        beginTime        = time.time()
        count            = 0  # the callback doesn't get this value
        if n is None:
            n       = len(self._depen)

        # use a callback to handle updates
        def update(theta):
            nonlocal count, lastTime, beginTime, updateDelay, maxTime, n

            count += 1
            nowTime = time.time()
            if (nowTime - lastTime) > updateDelay:
                print( '{} {:5}: '.format('MINM', count), end='' )
                print( self.print( theta ), end='' )

                lp = self.lnprob( theta )
                if n > 0:
                    print( ' ({:.2f}/n)'.format(lp/n) )
                else:
                    print( ' ({:.2f})'.format(lp) )

                lastTime = nowTime
                sys.stdout.flush()

            return ((nowTime - beginTime) > maxTime) or (count > maxIters)

        # repeatly minimize, as it likes to terminate early for unknown reasons
        while ((time.time() - beginTime) < maxTime) and (count < maxIters):

            results = spop_min( lambda x: -self.lnprob(x), x0=guess, callback=update )
            if (results['x'] == guess).all():
                break       # catch stalls

            guess = results['x']

        return results['x'], count


class Trendline(EmceeModel):
    """Fit a Trendline to the data. Defined as b*exp( m*x )."""

    def __init__(self, indep=list(), depen=list(), guess=list(), wiggle=list(), minlnstd=None, dist='gauss', \
            clip=0.6931471805599453, fade=0.34657359027997264, prior='invtan'):
        """Initialize ourselves.
        indep = An iterable consisting of the independent data, finite in length. Doesn't have to be
           one-dimensional. Technically optional, practically required.
        depen = An interable consisting of the dependent data, finite in length. The default 
           routines assume this is one-dimensional. Technically optional, practically required.
        guess = An iterable of length 3 that contains a guess at the maximal likelihood. Optional.
        wiggle = An iterable of length 3 that determines the std.dev of the Gaussian noise 
           applied to the guess. Optional.
        minlnstd = The minimal log standard deviation allowed. If blank, arbitrarily high precision
           is allowed.
        dist = The probability distribution used by lnlike() to evaluate model(). See 
           EmceeModel.__init_() for details.
        clip = The maximum magnitude allowed for the slope, before the likelihood drops to zero.
        fade = The maximum magnitude before the prior for linear interpolation is applied. If greater
           than clip, no fading is applied.
        prior = The prior to apply to the slope. Can be one of 'invtan' or 'flat'."""

        # let our parent do most of the heavy lifting
        EmceeModel.__init__(self, indep, depen, guess, wiggle, minlnstd, dist)

        # tweak the guess values (m, b, log(std.dev))
        if len(guess) == 3:       # perfect match
            self._guess   = guess
        elif len(guess) == 2:     # no stdev
            self._guess   = guess + [3]
        else:                     # default
            self._guess   = [0.0, 10, 3]

        if len(wiggle) == 3:       # perfect match
            self._wiggle   = wiggle
        elif len(wiggle) == 2:     # no stdev
            self._wiggle   = wiggle + [3]
        else:                     # default
            self._wiggle   = [0.1,  3, 3]

        self._clip    = np.abs(clip)        # prior variables
        self._fade    = np.abs(fade)
        if self._clip > self._fade:
            self._divisor = -np.log( self._clip - self._fade )
        else:
            self._divisor = 0               # a nonsense value that won't break anything

        if prior == 'invtan':               # handle the prior and any normalization factor

            if np.isfinite(self._clip) and (self._clip > self._fade):
                fadep1sqrt    = np.sqrt( self._fade**2 + 1 )
                clipp1sqrt    = np.sqrt( self._clip**2 + 1 )
                self._norm    = self._fade/fadep1sqrt + (fadep1sqrt*clipp1sqrt - (1 + self._clip*self._fade))/( (self._clip-self._fade)*fadep1sqrt )
            elif np.isfinite(self._clip):
                clipp1sqrt    = np.sqrt( self._clip**2 + 1 )
                self._norm    = self._clip/clipp1sqrt
            else:
                self._norm    = 0

            self._invtan = True             # since there's currently only two priors, just use a boolean

        else:

            if np.isfinite(self._clip) and (self._clip > self._fade):
                self._norm    = 2*self._fade + (self._clip - self._fade)
            elif np.isfinite(self._clip):
                self._norm    = 2*self._clip
            else:
                self._norm    = 0

            self._invtan = False


        if np.isfinite(self._norm) and (self._norm > 0):
            self._norm = -np.log( self._norm )
        else:
            self._norm = 0              # no normalization if we're truly flat

    def model_lnprior(self, theta):
        """Calculate the prior probability of the given parameter set.
        Returns the log probability."""

        assert len(theta) == 2
        m, b = theta

        # b must be positive
        if (b < 0):
            return -np.inf
    
        # clip the values if they fall above a given range
        if np.abs(m) > self._clip:
            return -np.inf

        # or fade them if they're above another
        if np.abs(m) > self._fade:
           m_prior = np.log( self._clip-np.abs(m) ) + self._divisor
        else:
           m_prior = 0

        # or switch out the main prior
        if self._invtan:
            m_prior += -1.5*np.log(1 + m*m) + np.log(.5)

        return m_prior + self._norm

    def model(self, theta, indep=list()):
        """For the given parameters and independent params, output the dependent values.
        If data is not supplied, use the dataset supplied with the constructor.
        Returns the predicted output."""

        # handle the data side of things
        if len(indep) < 1:
            indep = self._indep
            if len(indep) < 1:
                return -np.inf

        assert len(theta) == 2
        m, b = theta

        return b*np.exp( m*indep )

    def print(self, theta):
        """Print out the given parameter set in a sensible way."""

        assert len(theta) == 3
        m, b, ln_σ = theta
        return f"m={m:.3e}, b={b:.3e}, σ={np.exp(ln_σ):.2e}"

class TrendlineOff(EmceeModel):
    """Fit a Trendline to the data plus an offset. Defined as b*exp( m*x )."""

    def __init__(self, indep=list(), depen=list(), guess=list(), wiggle=list(), minlnstd=None, dist='gauss', \
            clip=0.6931471805599453, fade=0.34657359027997264, prior='invtan'):
        """Initialize ourselves.
        indep = An iterable consisting of the independent data, finite in length. Doesn't have to be
           one-dimensional. Technically optional, practically required.
        depen = An interable consisting of the dependent data, finite in length. The default 
           routines assume this is one-dimensional. Technically optional, practically required.
        guess = An iterable of length 3 that contains a guess at the maximal likelihood. Optional.
        wiggle = An iterable of length 3 that determines the std.dev of the Gaussian noise 
           applied to the guess. Optional.
        minlnstd = The minimal log standard deviation. If blank, arbitrarily high precision
           is allowed.
        dist = The probability distribution used by lnlike() to evaluate model(). See 
           EmceeModel.__init_() for details.
        clip = The maximum magnitude allowed for the slope, before the likelihood drops to zero.
        fade = The maximum magnitude before the prior for linear interpolation is applied. If greater
           than clip, no fading is applied.
        prior = The prior to apply to the slope. Can be one of 'invtan' or 'flat'."""

        # let our parent do most of the heavy lifting
        EmceeModel.__init__(self, indep, depen, guess, wiggle, minlnstd, dist)

        # tweak the guess values (m, log(b), logit(offset), log(stdev))
        if len(guess) == 4:       # perfect match
            self._guess   = guess
        elif len(guess) == 3:     # no stdev
            self._guess   = guess + [3]
        else:                     # default
            self._guess   = [0.0, 3, 0, 3]

        if len(wiggle) == 4:       # perfect match
            self._wiggle   = wiggle
        elif len(wiggle) == 3:     # no stdev
            self._wiggle   = wiggle + [3]
        else:                     # default
            self._wiggle   = [0.1,  1, 1, 3]

        self._clip    = np.abs(clip)        # prior variables
        self._fade    = np.abs(fade)
        if self._clip > self._fade:
            self._divisor = -np.log( self._clip - self._fade )
        else:
            self._divisor = 0               # a nonsense value that won't break anything

        if prior == 'invtan':               # handle the prior and any normalization factor

            if np.isfinite(self._clip) and (self._clip > self._fade):
                fadep1sqrt    = np.sqrt( self._fade**2 + 1 )
                clipp1sqrt    = np.sqrt( self._clip**2 + 1 )
                self._norm    = self._fade/fadep1sqrt + (fadep1sqrt*clipp1sqrt - (1 + self._clip*self._fade))/( (self._clip-self._fade)*fadep1sqrt )
            elif np.isfinite(self._clip):
                clipp1sqrt    = np.sqrt( self._clip**2 + 1 )
                self._norm    = self._clip/clipp1sqrt
            else:
                self._norm    = 0

            self._invtan = True             # since there's currently only two priors, just use a boolean

        else:

            if np.isfinite(self._clip) and (self._clip > self._fade):
                self._norm    = 2*self._fade + (self._clip - self._fade)
            elif np.isfinite(self._clip):
                self._norm    = 2*self._clip
            else:
                self._norm    = 0

            self._invtan = False


        if np.isfinite(self._norm) and (self._norm > 0):
            self._norm = -np.log( self._norm )
        else:
            self._norm = 0              # no normalization if we're truly flat

    def model_lnprior(self, theta):
        """Calculate the prior probability of the given parameter set.
        Returns the log probability."""

        assert len(theta) == 3
        m, ln_b, logit_o = theta

        # clip the values if they fall above a given range
        if np.abs(m) > self._clip:
            return -np.inf

        # or fade them if they're above another
        if np.abs(m) > self._fade:
           m_prior = np.log( self._clip-np.abs(m) ) + self._divisor
        else:
           m_prior = 0

        # or switch out the main prior
        if self._invtan:
            m_prior += -1.5*np.log(1 + m*m) + np.log(.5)

        return m_prior + self._norm

    def model(self, theta, indep=list()):
        """For the given parameters and independent params, output the dependent values.
        If data is not supplied, use the dataset supplied with the constructor.
        Returns the predicted output."""

        assert len(theta) == 4
        m, ln_b, _, _ = theta
        b = np.exp( ln_b )

        return b*np.exp( m*indep )

    def lnlike(self, theta, indep=list(), depen=list(), consolidate=True):
        """Calculate the likelihood of the given parameter set.
        indep = the independent data. If left blank, the independent data passed to the
           constructor is used.
        depen = the predicted data. If left blank, the data passed to the
           constructor is used.
        consolidate = If True, combine all likelihoods together; if False, leave
           separate. Handy for evaluating predictions.
        Returns the log probability."""

        assert len(theta) == 4

        if (len(depen) < 1):
            indep = self._indep
            depen = self._depen
        elif len(indep) != len(depen):
            indep = self._indep
        assert len(indep) == len(depen)

        m, ln_b, logit_o, ln_σ = theta
        b = np.exp( ln_b )
        o = self.logistic( logit_o )
        σ = np.exp( ln_σ )

        yp = b*np.exp( m*indep )
        if len(yp) != len(indep):           # these must be the same length
            return -np.inf
        if not np.isfinite(yp).all():
            return -np.inf                  # must be finite!

        retVal = self._dist( depen + o, yp, σ )
        if not (np.isfinite( retVal )).all():
            return -np.inf                  # filter out non-finite values

        if consolidate:
            return np.sum(retVal)
        else:
            return retVal


    def print(self, theta):
        """Print out the given parameter set in a sensible way."""

        assert len(theta) == 4
        m, ln_b, logit_o, ln_σ = theta
        return f"m={m:.3e}, b={np.exp(ln_b):.2e}, o={self.logistic(logit_o):.2e}, σ={np.exp(ln_σ):.2e}"



class LogGauss(EmceeModel):
    """Fit a Log Gaussian curve to the data."""

    def __init__(self, indep=list(), depen=list(), guess=list(), wiggle=list(), minlnstd=None, dist='gauss', \
        culm=True):
        """Initialize ourselves.
        indep = An iterable consisting of the independent data, finite in length. Doesn't have to be
           one-dimensional. Technically optional, practically required.
        depen = An interable consisting of the dependent data, finite in length. The default 
           routines assume this is one-dimensional. Technically optional, practically required.
        guess = An iterable of length 5 that contains a guess at the maximal likelihood. Optional.
        wiggle = An iterable of length 5 that determines the std.dev of the Gaussian noise 
           applied to the guess. Optional.
        minlnstd = The minimal log standard deviation allowed. If blank, arbitrarily high precision
           is allowed.
        dist = The probability distribution used by lnlike() to evaluate model(). See 
           EmceeModel.__init_() for details.
        culm = A boolean value flagging whether the dataset is cumulative or daily cases.
           Defaults to cumulative."""

        # let our parent do most of the heavy lifting
        EmceeModel.__init__(self, indep, depen, guess, wiggle, minlnstd, dist)

        self._culm     = culm

        # tweak the guess values (b, ln_mu, sigma, loc, ln_σ)
        if len(guess) == 5:       # perfect match
            self._guess   = guess
        elif len(guess) == 4:     # no stdev
            self._guess   = guess + [10]
        elif len(guess) == 3:     # no stdev+size
            self._guess   = [1000] + guess + [10]
        else:                     # default
            self._guess   = [1000, 10, 0.1, 10, 10]

        if len(wiggle) == 5:       # perfect match
            self._wiggle   = wiggle
        elif len(wiggle) == 4:     # no stdev
            self._wiggle   = wiggle + [3]
        elif len(wiggle) == 3:     # no stdev+size
            self._wiggle   = [200] + wiggle + [3]
        else:                     # default
            self._wiggle   = [200, 3, .01, 5, 3]


    def model_lnprior(self, theta):
        """The prior for the given model parameters.
        Returns the log likelihood."""

        assert len(theta) == 4
        b, ln_mu, sigma, loc = theta
    
        # b must be at least half a person, and sigma can't be too low
        if (b < 0.5) or (sigma < 1e-4):
            return -np.inf
    
        # place reasonable limits
        if (b > 1e10) or (ln_mu > 7.05e2): 
            return -np.inf
    
        if (np.abs(loc) > 3e2):
            return -np.inf

        # favour low std.devs and values of b
        return -2*np.log(b) - np.log(sigma)

    def model(self, theta, indep=list(), culm=None):
        """For the given parameters and independent params, output the dependent values.
        If data is not supplied, use the dataset supplied with the constructor.
        culm = A boolean that flags whether or not the data is in cumulative or daily cases.
           If None, use the value passed to the constructor.
        Returns the predicted output."""

        if culm is None:
            culm = self._culm

        # handle the data side of things
        if len(indep) < 1:
            indep = self._indep
            if len(indep) < 1:
                return -np.inf

        assert len(theta) == 4
        b, ln_mu, sigma, loc = theta
    
        if culm:
            return b*spst.lognorm.cdf(indep, sigma, loc=loc, scale=np.exp(ln_mu))
        else:
            return b*spst.lognorm.pdf(indep, sigma, loc=loc, scale=np.exp(ln_mu))

    def print(self, theta):
        """Print out the given parameter set in a sensible way."""
    
        assert len(theta) == 5
        b, ln_mu, sigma, loc, ln_σ = theta
        return f"b={b:.3e}, mu={np.exp(ln_mu):.3e}, s={sigma:.3e}, loc={loc:.3e}, σ={np.exp(ln_σ):.2e}"


class LogGausses(EmceeModel):
    """Fit multiple Log Gaussian curves to the data."""

    def __init__(self, indep=list(), depen=list(), guess=list(), wiggle=list(), minlnstd=None, dist='gauss', \
        curves=8):
        """Initialize ourselves. Unlike LogGauss, this is cumulative-only.
        indep = An iterable consisting of the independent data, finite in length. Doesn't have to be
           one-dimensional. Technically optional, practically required.
        depen = An interable consisting of the dependent data, finite in length. The default 
           routines assume this is one-dimensional. Technically optional, practically required.
        guess = An iterable that contains a guess at the maximal likelihood. Optional.
        wiggle = An iterable that determines the std.dev of the Gaussian noise 
           applied to the guess. Optional.
        minlnstd = The minimal log standard deviation allowed. If blank, arbitrarily high precision
           is allowed.
        dist = The probability distribution used by lnlike() to evaluate model(). See 
           EmceeModel.__init_() for details.
        curves = The maximal number of curves to combine. Must be positive and finite."""

        # let our parent do most of the heavy lifting
        EmceeModel.__init__(self, indep, depen, guess, wiggle, minlnstd, dist)

        self._curv     = curves

        # have we gotten the appropriate number of parameters?
        params   = 4*curves + 1

        if len(guess) == params:        # perfect match
            self._guess  = guess
        elif len(guess) == params - 1:  # missing stdev
            self._guess  = guess + [10]
        elif len(guess) == 4:           # just the first curve's info
            self._guess  = list(guess)
            for _ in range(1,curves):
                self._guess.extend( [0] + list(guess[1:]) )
            self._guess.append( 10 )
        else:                           # default values
            self._guess = [1000, 10, 0.1, 10]  # (ln_b/log_prop, ln_mu, ln_sigma, loc), sigma
            for _ in range(1,curves):
                self._guess.extend( [0, 10, 0.1, 10] )
            self._guess.append( 10 )

        if len(wiggle) == params:        # perfect match
            self._wiggle  = wiggle
        elif len(wiggle) == params - 1:  # missing stdev
            self._wiggle  = wiggle + [3]
        elif len(wiggle) == 4:           # just the first curve's info
            self._wiggle  = list(wiggle)
            for _ in range(1,curves):
                self._wiggle.extend( [3] + list(wiggle[1:]) )
            self._wiggle.append( 3 )
        else:                           # default values
            self._wiggle = [200, 3, .01, 5]  # (ln_b/ln_prop, ln_mu, ln_sigma, loc), sigma
            for _ in range(1,curves):
                self._wiggle.extend( [3, 3, .01, 5] )
            self._wiggle.append( 3 )


    def model_lnprior(self, theta):
        """The prior for the given model parameters.
        Returns the log likelihood."""

        # start pulling variables out
        n   = len(theta) >> 2
        off = n << 2
        b   = theta[0]
        
        ln_scale = theta[1:1+off:4]
        sigma    = theta[2:2+off:4]
        mu       = theta[3:3+off:4]
        ln_prop  = theta[4:0+off:4]
        bs       = b*np.cumprod(self.logistic( ln_prop ))
    
        if n > self._curv:
            return -np.inf

        # place reasonable limits
        if (b < 0.5) or (b > 1e12):
            return -np.inf

        if sum(map( lambda x: x < 0.5, bs )) > 0:
            return -np.inf

        if sum(map( lambda x: x > 7.05e2, ln_scale)) > 0: 
            return -np.inf
    
        # sigma can't be too low
        if sum(map( lambda x: x < 1e-4, sigma )) > 0:
            return -np.inf
    
        if (np.abs(mu) > 3e2).any():
            return -np.inf

        if sum(map( lambda x: x > 7.05e2, ln_prop)) > 0: 
            return -np.inf
    
        # favour low std.devs, b's, and n's
        return sum(np.log(sigma)) - 2*np.log( b*n )

    def model(self, theta, indep=list()):
        """For the given parameters and independent params, output the dependent values.
        If data is not supplied, use the dataset supplied with the constructor.
        Returns the predicted output."""

        # handle the data side of things
        if len(indep) < 1:
            indep = self._indep
            if len(indep) < 1:
                return -np.inf

        # start pulling variables
        n   = len(theta) >> 2
        off = n << 2

        b  = theta[0]
        bs = b*np.cumprod(self.logistic( theta[4:0+off:4] ))
        for idx in range(n):
    
            if idx == 0:
                retVal  = b*spst.lognorm.cdf(indep, theta[2], loc=theta[3], scale=np.exp(theta[1]))
            else:
                off     = 4*idx
                retVal += bs[idx-1]*spst.lognorm.cdf(indep, theta[2+off], \
                        loc=theta[3+off], scale=np.exp(theta[1+off]))

        return retVal

    def print(self, theta):
        """Print out the given parameter set in a sensible way."""
    
        n   = len(theta) >> 2
        off = n << 2

        b     = theta[0]
        if n < 3:
            scale = np.exp( theta[1:1+off:4] )
            sigma = theta[2:2+off:4]
            mu    = theta[3:3+off:4]
            bs    = b*self.logistic( theta[4] )
        else:
            scale = np.quantile( np.exp( theta[1:1+off:4] ), [0,.5,1] )
            sigma = np.quantile( theta[2:2+off:4], [0,.5,1] )
            mu    = np.quantile( theta[3:3+off:4], [0,.5,1] )
            bs    = np.quantile( [b] + list(b*np.cumprod(self.logistic( theta[4:0+off:4] ))), [0,.5,1] )

        if n < 2:
            return f"n={n}, b={b+.5:.0f}, scale={scale[0]:.3e}, s={sigma[0]:.3e}, mu={mu[0]:.3e}, σ={np.exp(theta[-1]):.2e}"
        elif n < 3:
            return f"n={n}, b={b+.5:.0f}/{bs+.5:.0f}, scale={scale[0]:.2e}/{scale[1]:.2e}, s={sigma[0]:.2e}/{sigma[1]:.2e}," + \
                f" mu={mu[0]:.2e}/{mu[1]:.2e}, σ={np.exp(theta[-1]):.2e}"
        else:
            retVal  = f"n={n}, b={bs[0]+.5:.0f}/{bs[1]+.5:.0f}/{bs[2]+.5:.0f}"
            retVal += f", scale={scale[0]:.2e}/{scale[1]:.2e}/{scale[2]:.2e}"
            retVal += f", s={sigma[0]:.2e}/{sigma[1]:.2e}/{sigma[2]:.2e}"
            retVal += f", mu={mu[0]:.2e}/{mu[1]:.2e}/{mu[2]:.2e}"
            return retVal + f", σ={np.exp(theta[-1]):.2e}"


class SEIR_Variable(EmceeModel):
    """Fit a SEIR model to the data. The 'Variable' part comes from the way it allows
    social distancing and other health measures to mute the spread. It incorporates
    day-of-week effects, too. Interpolation is done via interpolating cubic splines that
    have their first derivative clamped to zero outside their domain."""

    @staticmethod
    def setup_coarse(indep, res=21):
        """Create a key value for the coarse spline."""
        assert (len(indep) > 0)

        # work out the coarse interpolant locations 
        steps = int( np.ceil( (max(indep) - min(indep)) / res ) )
        return np.linspace(0, steps, steps+1)*res + min(indep)

    @staticmethod
    def SEIR_interp(t, Y, N, coarse, fine, beta, gamma, zeta):
        """A helper function for solving the SEIR IVP.
        t = The time to interpret the data at. Days since Unix epoch!
        Y = The current state of the system. In [S, E, I, R, culm(I)] format.
        N = The total population of the area.
        coarse = A cubic spline for coarse social distancing info.
        fine = A cubic spline for day-of-week distancing effects.
        beta, gamma, zeta = Standard SEIR parameters.
        Returns the slope at time T in [S, E, I, R, culm(I)] format."""

        t_clip   = np.clip( t, coarse.x[0], coarse.x[-1] )
        coarse_e = SEIR_Variable.logistic(np.clip(coarse(t_clip), -7e2, 7e2))

        beta_p = beta * coarse_e * fine(t)
        first  = beta_p*Y[1]*Y[0]/N
        second = gamma*Y[1]
        third  = zeta*Y[2]

        return [-first, first-second, second-third, third, second]


    def _zero_storage(self, guess=list(), wiggle=list()):
        # blank out all storage

        indep = list()
        depen = list()
        coarse_x = list()

        if len(guess) > 0:
            guess = list(guess)   # we need these stored somewhere
        else:
            guess = list()

        if len(wiggle) > 0:
            wiggle = list(wiggle)
        else:
            wiggle = list()

        return indep, depen, coarse_x, guess, wiggle, 0

    def _clean_data(self, indep, depen, guess=list(), wiggle=list(), res=-1 ):
        # add a dataset to storage

        if (len(depen) > 1) and (len(indep) > 1) and \
                (len(depen) == len(indep)):

            # This works best with at least one case at the start, so adjust the data appropriately
            idx = -1
            for i,v in enumerate(depen):
                if v > 0:
                    idx = i
                    break
            else:
                return self._zero_storage( guess, wiggle )

            # if otherwise successful, set up the rest
            if res < 1:
                res = self._res

            coarse_x = self.setup_coarse( indep[idx:], res )

            if len(guess) == 4:
                guess  = list(guess) + list( np.linspace(2,-2,len(coarse_x)) )   # coarse interpolants in logistic space
            elif len(guess) == 4 + len(coarse_x):
                guess  = list(guess)
            else:
                guess  = [-.43, -1.5, -0.36, 2.3] + list( np.linspace(2,-2,len(coarse_x)) )
                        # beta, gamma, zeta, exposed, σ, all in logspace

            guess = guess + [4]     # add stdev

            if len(wiggle) == 4:
                wiggle = list(wiggle) + [1]*len(coarse_x)
            elif len(wiggle) == 4 + len(coarse_x):
                wiggle = list(wiggle)
            else:
                wiggle = [.1, .1, .1, .1] + [1]*len(coarse_x)

            wiggle = wiggle + [1]   # stdev

            return indep[idx:], depen[idx:], coarse_x, guess, wiggle, idx

        else:
            return self._zero_storage( guess, wiggle )


    def __init__(self, pop, indep=list(), depen=list(), guess=list(), wiggle=list(), minlnstd=None, dist='gauss', \
            res=21, dow=[0.99267289, 1.01766002, 1.00237905, 1.04044993, 1.11594461, 0.99913655, 0.83175695]):
        """Initialize the model.
        pop = The total population of the area we're modelling.
        indep = An iterable consisting of the independent data, finite in length. Doesn't have to be
           one-dimensional. Technically optional, practically required.
        depen = An interable consisting of the dependent data, finite in length. The default 
           routines assume this is one-dimensional. Technically optional, practically required.
        guess = Initial values for the three standard SEIR coefficients + the initial number exposed. In log space!
        wiggle = Standard deviation of the Gaussian used to wiggle those points.
        minlnstd = The minimal log standard deviation allowed. If blank, arbitrarily high precision
           is allowed.
        dist = The probability distribution used by lnlike() to evaluate model(). See 
           EmceeModel.__init_() for details.
        res = The resolution of the coarse spline. Default is seven days.
        dow = A 7-item list that represents fine day-of-week effects. The default comes from Canadian
           mass transit usage, according to Apple. First item = Monday."""

        assert pop > 0
        self._pop = pop

        assert res > 0
        self._res = res

        # generate the fine spline (-2.5 = Monday, in days since Unix epoch)
        self._fine = CubicSpline( np.linspace(-3.5, 3.5, 8), [dow[-1]] + list(dow), bc_type='periodic' )

        # add the data, and fix all associated values
        i, d, self._coarse_x, g, w, self._idx = self._clean_data(indep, depen, guess, wiggle)

        # pass those values to the parent
        EmceeModel.__init__(self, i, d, g, w, minlnstd, dist)


    def offset(self):
        """Retrieve the starting offset into the original dataset."""
        return self._idx

    def seed(self, n=None, guess=list(), wiggle=list(), indep=list(), depen=list(), rstate=None):
        """Generate a seed value for this model. A wrapper around the parent routine.
        n = the number of seed values to create.
        guess = an optional override to the default guess iterable.
        wiggle = an optional override to the default wiggle iterable.
        indep, depen = Some optional data to test against. Can help weed out invalid parameters.
        rstate = If present, seed numpy's RandomState RNG routines with this value.
        Returns (seed, trials), where seed is a list of parameter seeds and trials is
           the number of trials necessary to generate those seeds."""

        # as a convenience, pad the guess/wiggle if they're short
        if len(guess) == 4:
            guess = list(guess) + list( np.linspace(2,-2,len(self._coarse_x)) ) + [4]

        if len(wiggle) == 4:
            wiggle = list(wiggle) + [1]*len(self._coarse_x) + [3]

        return EmceeModel.seed( self, n, guess=guess, wiggle=wiggle, indep=indep, depen=depen, rstate=rstate )
            
    def model_lnprior(self, theta):
        """Calculate the prior probability of the given parameter set.
        Returns the log probability."""

        lbeta, lgamma, lzeta, lexposed = theta[:4]   # extract the five main parameters, each in logspace
        if (np.abs(theta) > 7.05e2).any():          # prevent math over/underflow errors due to exponentiation
            return -np.inf

        if (np.exp(theta[:3]) < 1e-4).any():        # the model seems to break in this parameter regime
            return -np.inf

        # favour close fits and small numbers of exposed, plus penalize small cubic weights
        return -lexposed + sum( np.log(self.logistic(theta[4:])) )


    def model(self, theta, indep=list(), init_inf=1, extend=None, coarse_x=list()):
        """Calculate the model output, for the given parameter set.
        indep = An iterable consisting of the independent data, finite in length. Doesn't have to be
           one-dimensional. Will used stored values if absent.
        init_inf = The initial number of infected.
        extend = How to extend coarse_x into the future. Should be a float between 0 and 1, 
           exclusive, if supplied. None = repeat last value.
        coarse_x = An internal value used in calculation. Expert use only.
        Returns the output of the model, plus either True if the ODE was
           successfully solved or a string if not."""

        # no indep? Use the internal values
        if len(indep) < 1:
            indep    = self._indep

        if init_inf < 1:
            init_inf = 1
        
        # still got nonsense? Bail
        if len(indep) < 1:
            return list() # , "Need at least one independent value."

        # pull this out so we can modify it
        coarse_y = theta[4:]

        # trust coarse_x if given
        if len(coarse_x) < 1:

            coarse_x = self._coarse_x

            # extend the spline if necessary
            if (type(extend) == float) and (extend > 0) and (extend < 1):
                coarse_x = list(coarse_x) + [coarse_x[-1] + self._res]
                coarse_y = list(coarse_y) + [self.logit( extend )]

        # don't make this fatal, but refuse to carry on otherwise
        if len(coarse_x) != len(coarse_y):
            print( "ERROR: coarse_x does not have the correct length " + \
                f"({len(coarse_x)} should be {len(coarse_y)})" )
            return list()

        beta, gamma, zeta, exposed = np.exp(theta[:4])   # convert four parameters to linear space

        # create the coarse spline
        coarse = CubicSpline( coarse_x, coarse_y, bc_type='clamped' )

        # setup our IVP helper
        helper = lambda t,y: self.SEIR_interp( t, y, self._pop, coarse, self._fine, \
                beta, gamma, zeta )

        # create the model by solving an IVP
        solution = {'y':[[],[],[],[],[]], 'success':False, \
                'message':"solve_ivp(method='Radau') failed to converge on a solution."}
        try:
            solution = solve_ivp( helper, (min(indep),max(indep)), \
                [self._pop - exposed - init_inf, exposed, init_inf, 0, init_inf], \
                t_eval=indep, vectorized=False, method='Radau' )

        except ValueError:    # testing shows that if Radau fails, other algs are likely/certain to fail.
            pass

        # catch failed sim runs
        if (len(solution['y'][4]) != len(indep)) or not solution['success']:
            print( f"ERROR: could not fit model for {theta}", end='' )
            print( f", {solution['message']}." )
            sys.stdout.flush()
            return list()

        if (np.diff(solution['y'][4]) < 0).any():        # weed out case count reversals
            return list()

        return solution['y'][4]


    def print(self, theta):
        """Print out the given parameter set in a sensible way."""
    
        assert len(theta) > 4
        beta, gamma, zeta, exposed = np.exp(theta[:4])   # convert everything to linear space
        quants = np.quantile( theta[4:-1], [0, .5, 1] )
        σ = np.exp(theta[-1])

        return f"β/γ/ζ = {beta:.3e}/{gamma:.3e}/{zeta:.3e}, E={exposed:.2e}, σ={σ:.2e}," + \
                f" c(t)∈[{quants[0]:.1f},{quants[2]:.1f}]≅{quants[1]:.1f}"


##### METHODS

def valid_data(x):
    return np.isfinite(x) and (x >= 0)

def clean_series( x, y, period=0.9*86400, start=None, reversals=None ):
    """Clean up a data series by removing duplicate points.
    x, y       = The incoming independent and dependent data series.
    period     = The period of time after which points are not considered duplicates.
    start      = Clip all times before this. Disable with None.
    reversals  = Remove negative or zero deltas by adjusting prior cases downward. Disable 
                   with None, 'inc' means delete values until the next is positive,
                  'scale' scales prior values to ensure L1 continuity."""
            
    paired = [[v,y[i]] for i,v in enumerate(x) if valid_data(v) and valid_data(y[i])]
    retVal = list()
    last_i = 0
    for i,v in enumerate(paired):
        
        # skip datapoints before the start
        if (start is not None) and (v[0] < start):
            continue

        # if starting, or there's been a change in value, or the minimal period has elapsed
        if (i == 0) or (v[1] != paired[last_i][1]) or (v[0] - paired[last_i][0] > period):

            # the first value is always added
            if i == 0:
                retVal.append( v )
                last_i = i

            # if "inc" is enabled and the last value is greater/equal to the current, don't add it
            elif (reversals == 'inc') and (len(retVal) > 0) and (v[1] <= retVal[-1][1]):
                continue

            # otherwise, do a little tapdancing if 'scale' is enabled
            elif (reversals == 'scale') and (v[1] < retVal[-1][1]):

                # figure out the slope on the high side, if possible
                if (len(paired) > (i+1)) and ((paired[i+1][0] - v[0]) > 0):
                    slope_high = (paired[i+1][1] - v[1]) / (paired[i+1][0] - v[0])
                else:
                    slope_high = 0

                #  and on the low side
                if (len(retVal) > 1) and ((retVal[-2][0] - retVal[-1][0]) > 0):
                    slope_low = (retVal[-2][1] - retVal[-1][1]) / (retVal[-2][0] - retVal[-1][0])
                else:
                    slope_low = 0

                #  the mean is our target slope, which we use to adjust the current value
                slope = (slope_high + slope_low) * 0.5

                #  and then scale all prior values by that amount as well
                target = v[1] - (v[0] - retVal[-1][0]) * slope
                scale = target / retVal[-1][1]
            
                retVal = [(u[0], scale*u[1]) for u in retVal]
                retVal.append( v )
                last_i = i

            # all else fails? Just append it
            else:
                retVal.append( v )
                last_i = i
                                                        
    return list(zip( *retVal ))


