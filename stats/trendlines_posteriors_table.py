#!/usr/bin/env python3

##### INCLUDES

import argparse
import gc
import gzip
from multiprocessing import Pool
import numpy as np
import os
import pandas as pd
import pickle
import sys
import time


##### VARIABLES


##### METHODS
def print_mem( flush=True ):
    membytes = sys.getallocatedblocks() << 12
    if membytes < (1 << 30):
        print( f" [{membytes/(1<<20):.1f}MB", end='')
    else:
        print( f" [{membytes/(1<<30):.1f}GB", end='')
    print( f" {gc.get_count()}]", flush=flush )

def status( dataset=None, file=None ):
    global todo, total

    if (file is not None):
        progress = total - len(todo)
        print(f"= Started loading {file} ({progress}/{total})", end='')
        print_mem()

    if (dataset is not None):

        days = dataset[0]
        kind, idx = dataset[1]

        print(f"> ({kind}/{idx}@{days})", end='')
        print_mem()


def load_bundle( file ):
    try:
      with gzip.open( file, 'rb' ) as xfile:
        return pickle.load( xfile )
    except KeyboardInterrupt:
        return None
    except:
#      os.remove( file )
      t, v, tb = sys.exc_info()
      print( f'= Detected corrupt file {file}. Exception {t}, {str(v)}, line {tb.tb_lineno}.' )
      return None

def evaluate( data ):

    # temporary storage
    dataset = list()

    # load one up
    for row in data['pos']:
          for idx in np.random.choice(len(row[4]), size=args.samples):
                
            dataset.append( [data['index'][0],          # window size
                             row[0],                    # n
                             row[2],                    # last day within window
                             data['index'][1][0],       # type
                             data['index'][1][1],       # series
            
                             row[4][idx] / row[0],      # log likelihood / n
            
                             row[3][idx][0],            # m / b / ln_σ
                             row[3][idx][1],
                             row[3][idx][2]] )
                
    return dataset, data['index'], data['params']


##### MAIN

parser = argparse.ArgumentParser( description="Consolidate quality assessments and posteriors into a table.")
parser.add_argument( '-v',"--verbose", action='store_true', help="Give more verbose update messages." )

parser.add_argument( 'indir', metavar="FITDIR", default='generated/posteriors/0', \
                                help="The input directory containing fitting files." )
parser.add_argument( 'outdir', metavar="OUTDIR", default='generated/tables/0', \
                                help="The output directory of the resulting summary table." )

parser.add_argument( '-f',"--files", type=int, default=9, help="The maximum number of files to load into memory." )
parser.add_argument( '-T',"--threads", type=int, default=40, help="The number of threads to use. High numbers don't always help." )
parser.add_argument( '-n',"--samples", type=int, default=16, help="The number of random samples to draw from the posterior." )
parser.add_argument( '-M',"--thrdmult", type=float, default=1.5, help="Queue length = this times the number of threads." )
parser.add_argument( "--seed", type=int, default=1729, help="The random number seed used. 0 = use the default generated one." )

args = parser.parse_args()

# tweak the thread count
if args.threads <= 0:
        args.threads = len(os.sched_getaffinity(0))

# set the seed, if asked
if args.seed != 0:
        np.random.seed( args.seed )


dataset = list() # pooled copy
params  = dict() # enforce a consistent parameter set for the dataset

todo = os.listdir( args.indir )
np.random.shuffle( todo )           # scramble for better load-balance
total = len(todo)


# seperate routine for single-threaded computation
if args.threads == 1:

    for fit_file in todo:
        bundle = load_bundle(f"{args.indir}/{fit_file}")
        for idx,fit in enumerate(bundle):

            result = evaluate(fit)
            if result is None:
                continue

            array, meta, param = result
            if len(params) == 0:
                params = param
            if param == params:       # only save if params match
                dataset.extend( array )
            else:
                print( f"= WARNING: Dataset with different parameters was processed ({meta})." )

            status( dataset=meta )

# or, if we're multiprocessing
else:
  with Pool( processes=args.threads, maxtasksperchild=6 ) as pool:

    load_queue = list()         # use these for background loading
    proc_queue = list()
    bundle     = None           # storage to allow for task reordering

    while (len(todo) > 0) or (len(load_queue) > 0) or (len(proc_queue) > 0):

        # do a "pass" over the loading queue (but don't blot out an existing bundle)
        if (len(load_queue) > 0) and (bundle is None) and load_queue[0].ready():

            bundle = load_queue[0].get()
            load_queue.pop(0)

        # stuff the file queue
        while (len(todo) > 0) and (len(load_queue) < args.files):

            fit_file = todo.pop()
            load_queue.append(pool.apply_async( load_bundle, (f"{args.indir}/{fit_file}",) ))
            status( file=fit_file )

        # fill in the processing queue with results from the loading queue
        if (len(proc_queue) < int(args.thrdmult*args.threads)) and (bundle is not None):
            for idx,fit in enumerate(bundle):

                proc_queue.append(pool.apply_async( evaluate, (fit,) ))

            # flag that we're ready for another bundle
            bundle = None

        # do a pass over the processing queue
        for idx,state in enumerate(proc_queue):
            if state.ready():
        
                result = state.get()
                if result is None:
                    continue

                array, meta, param = result
                if len(params) == 0:
                    params = param
                if param == params:       # only save if params match
                    dataset.extend( array )
                else:
                    print( f"= WARNING: Dataset with different parameters was processed ({meta})." )

                status( dataset=meta )
                proc_queue.pop(idx)     # this won't muck up the iterator

        # take a rest, to give other threads some time to work
        time.sleep(0.1)

if args.verbose:
    print( f"= Converting {len(dataset)} rows to a pandas dataframe ...", end='', flush=True )
data = pd.DataFrame( dataset, columns=['window size','n','terminal time','type','series','log likeli/n','m','b','log(σ)'])
del dataset
if args.verbose:
    print( " done.", flush=True )

# figure out the proper filename
filename = f"{args.outdir}/posteriors."
if 'preset' in params:
    filename += f"{params['preset']}"
else:
    filename += f"{params['clip']}.{params['fade']}.{params['type']}.{params['minlnstd']}.{params['dist']}"
filename += f".{params['dataset']}.pickle.gz"

if args.verbose:
    print( f"= Writing {filename} ...", end='', flush=True )
with gzip.open( filename, 'wb', compresslevel=9 ) as zfile:
     pickle.dump( data, zfile )
if args.verbose:
    print( " done.", flush=True )

print( "= Finished.", flush=True )
