#!/usr/bin/env python3

##### INCLUDES

import gzip
import json
from multiprocessing import Pool, Manager
import numpy as np
import os
import pandas as pd
import pickle


##### VARIABLES
posterior_sample = 8        # gotta cut this right back


##### METHODS

def evaluate( file ):

    # temporary storage
    dataset = list()

    # only bother with posterior datasets
    if not file.startswith("posteriors.1.0."):
        return None
    
    # load one up
    try:
      with gzip.open( f'trendlines/{file}', 'rt' ) as gfile:

        data = json.load( gfile )
        
        for row in data['pos']:
            for idx in np.random.choice(len(row[4]), posterior_sample):
                
                dataset.append( [row[0],
                                 row[2],
                                 data['index'][0],
                                 data['index'][1],
            
                                 row[4][idx] / row[0],
            
                                 *row[3][idx]] ) # might as well dump the entire posterior in
                
      return dataset
    except:
      os.remove( f'trendlines/{file}' )
      print( f'Removed trendlines/{file}' )
      return None


##### MAIN

dataset = list() # pooled copy

todo = os.listdir("trendlines")
np.random.shuffle( todo )           # scramble for better load-balance


# fire up multiprocessing
with Pool(10) as pool:

    for result in pool.imap_unordered( evaluate, todo, 1 ):

        if result is None:
            continue

        dataset.extend( result )

data = pd.DataFrame( dataset, columns=['window size','terminal time','type','series','log likeli','b','ln_mu','sigma','loc','log(σ)'])
del dataset

with gzip.open(f'posteriors.1.0.table.pickle.gz', 'wb', compresslevel=9) as zipfile:
     pickle.dump( data, zipfile )
