#!/usr/bin/env python3

import json
import pandas as pd


# open it up
bc_status = json.load( open('bc_summary.json','rt') )

# extract the header
try:
    header  = list(bc_status['features'][0]['attributes'].keys())
    dataset = list()

    for row in bc_status['features']:
        dataset.append( [row['attributes'][x] for x in header] )

    out = pd.DataFrame(dataset, columns=header)
    out.to_csv('bc_summary.csv', index=False)

except:
    t, v, tb = sys.exc_info()
    print( f"extract_bc_stats: Exception {t}: {str(v)}, line {tb.tb_lineno}" )

