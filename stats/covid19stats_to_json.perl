#!/usr/bin/perl -w


##### VARIABLES

my $base  = $ARGV[0];
my $name = "alberta.covid19stats";
my $counter = 1;



##### MAIN

while (<STDIN>) {

	if( m|<script type="application/json"[^>]*>(.*?)</script>| ) {

		for (m|<script type="application/json"[^>]*>(.*?)</script>|) {

			my $filename = sprintf( "$base/$name.%02d.plotly.json", $counter );
			open( FH, ">", $filename )	
				or die "Error! Could not open $filename for writing! $!";

			print FH $_;
			close( FH );

			$counter++;
			}
		}

	elsif ( m|<script type="application/json"[^>]*>| ) {

		my $filename = sprintf( "$base/$name.%02d.plotly.json", $counter );
		open( FH, ">", $filename )	
			or die "Error! Could not open $filename for writing! $!";

		while(<STDIN>) {

			if( m|^(.*)</script>| ) {

				print FH $1;
				close( FH );

				$counter++;
				last;
				}
			else {
				print FH $_;
				}
			}

		}
	}
