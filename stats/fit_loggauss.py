#!/usr/bin/env python3

##### INCLUDES

from emcee_shared import *

import gzip
import json
from multiprocessing import Pool
import numpy as np
import os
import sys


##### VARIABLES

posSize = 2048    # size of the posterior



##### METHODS

def filename( kind, idx ):
    return f'trendlines/posteriors/1/posteriors.1.0.x.{kind}.{idx}.json.gz'

def evaluate( combo ):

    posterior = list()

    # figure out what our parameters are
    kind, idx = combo

    # skip files that already exist
    if os.path.isfile( filename(kind, idx) ):
         return None

    # clean up the series
    data = clean_series( dataset[kind][idx][0], dataset[kind][idx][1] )
    # deltas are very tough to fit with loggauss

    # start evaluating each time frame
    right = 0
    while right < len(data[0]):

        # ensure we're using a minimum of 3 days
        if (data[0][right] - data[0][0])/86400 < 3:
            right += 1
            continue

        # build and evaluate a model
        indep  = np.array(data[0][:right])/86400
        indep -= np.min(indep)

        depen  = data[1][:right]

        lg_model = LogGauss( indep=indep, depen=depen )
        pos, stats = lg_model.run_sampler( maxTime=30, updateDelay=np.inf, stepsize=32 )

        steps = posSize // len(pos)
        pos, stats = lg_model.run_sampler( seed=pos, maxIters=steps, updateDelay=np.inf, \
                burn=False, stepsize=steps )

        # start building the posterior
        np.random.shuffle( pos )   # sample from the posterior

        temp = [right, data[0][0], data[0][right-1], [], []]
        for theta in pos[:256]:

            temp[3].append( list(theta) )
            temp[4].append( lg_model.lnprob(theta) )

        posterior.append( temp )

        # finally, advance the window forward
        right += 1

    return (posterior, combo)

##### MAIN

# read off the index/step from the command line
try:
    idx  = int(sys.argv[1])
    step = int(sys.argv[2])
except:
    print("ERROR: Please supply an index and step size on the command line.")
    sys.exit(1)

# read in the dataset
with gzip.open( 'fitting_dataset.json.gz', 'rt', encoding="utf-8") as zipfile:
    dataset = json.load( zipfile )

# build up a to-do list
todo = list()
for kind_idx,kind in enumerate(dataset):
    for i,_ in enumerate(kind):
            todo.append( (kind_idx,i) )

# scramble it, deterministically
np.random.seed( 42 )
np.random.shuffle( todo )
np.random.seed()

# fire up multiprocessing
with Pool() as pool:

    for result in pool.imap_unordered( evaluate, todo[idx::step], 1 ):
#    for result in map( evaluate, todo[idx::step] ):

        if result is None:                                        # skip invalid results
            continue

        posterior, combo = result
        if (posterior is None) or (len(posterior) < 1):           # skip invalid results
            continue

        kind, idx = combo

        # gzip support, courtesy https://stackoverflow.com/questions/49534901/is-there-a-way-to-use-json-dump-with-gzip/49535758#49535758
        with gzip.open( filename( kind, idx ), 'wt', encoding="utf-8", compresslevel=9) as zipfile:
            json.dump( {'index':combo,'pos':posterior}, zipfile )
        print( combo )
