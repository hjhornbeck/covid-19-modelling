#!/usr/bin/env python3

import json
import gzip
import numpy as np
import pandas as pd

master = list()   # types are array indicies, sub-types are too, and it's [epoch, cases, deaths, active, recovered, tested]
meta   = {'pop':list(), 'names':list(), 'data':list()}   # use this for metadata, like population counts

def to_int(array):
    return [int(x) for x in array]

# type 0: Alberta LGAs
master.append( [] )
lgas = json.load( open('data_series.json','rt') )

for lga in lgas:
    master[-1].append( [lga[0], to_int(lga[1]), to_int(lga[4]), \
            to_int(lga[2]), to_int(lga[3]), []] )

del lgas

alberta_pop = pd.read_csv( 'lga_reference_table.tsv.csv', sep='\t', names=['lga','population'])
alberta_meta = json.load( open('data_meta.json','rt') )
meta['pop'].append( to_int(alberta_pop['population']) )
meta['names'].append( alberta_meta['names'] )

assert len(master[0]) == len(meta['pop'][0])
assert len(meta['names'][0]) == len(meta['pop'][0])


# type 1: Canada
master.append( [] )
meta['names'].append( list() )
areas = json.load( open('summary_series.json','rt') )

for area in areas:
    master[-1].append( [areas[area][0], to_int(areas[area][1]), to_int(areas[area][3]), \
            [], to_int(areas[area][2]), to_int(areas[area][4])] )
    meta['names'][-1].append( area )

del areas

# source: https://experience.arcgis.com/experience/2f1a13ca0b29422f9b34660f0b705043/
# (repatriated pop = number tested from https://www.canada.ca/en/public-health/services/diseases/2019-novel-coronavirus-infection.html)
# (Canada pop = sum of all other populations)
meta['pop'].append( [4067175, 4648055, 35151796, 1278365, 747100, 519715, 41785, 923600, 35940, 13448495, 142905, 8164360, 76, 1098355, 35870] )

assert len(master[1]) == len(meta['pop'][1])
assert len(meta['names'][1]) == len(meta['pop'][1])


# type 2: Ontario health regions
master.append( [] )
lgas = json.load( open('ontario_series.json','rt') )

for lga in lgas:
    master[-1].append( [lga[0], to_int(lga[1]), to_int(lga[2]), \
            to_int(lga[3]), [], []] )

del lgas

# these are straight from conposcovidloc_consolidate.py
meta['names'].append( ['Algoma Public Health Unit', 'Brant County Health Unit', 'Chatham-Kent Health Unit', 
    'Durham Region Health Department', 'Eastern Ontario Health Unit', 'Grey Bruce Health Unit',
    'Haldimand-Norfolk Health Unit', 'Haliburton, Kawartha, Pine Ridge District Health Unit',
    'Halton Region Health Department', 'Hamilton Public Health Services', 
    'Hastings and Prince Edward Counties Health Unit', 'Huron Perth District Health Unit', 
    'Kingston, Frontenac and Lennox & Addington Public Health', 'Lambton Public Health',
    'Leeds, Grenville and Lanark District Health Unit', 'Middlesex-London Health Unit', 
    'Niagara Region Public Health Department', 'North Bay Parry Sound District Health Unit', 'Northwestern Health Unit', 
    'Ottawa Public Health', 'Peel Public Health', 'Peterborough Public Health', 'Porcupine Health Unit', 
    'Region of Waterloo, Public Health', 'Renfrew County and District Health Unit', 'Simcoe Muskoka District Health Unit', 
    'Southwestern Public Health', 'Sudbury & District Health Unit', 'Thunder Bay District Health Unit',
    'Timiskaming Health Unit', 'Toronto Public Health', 'Wellington-Dufferin-Guelph Public Health', 'Windsor-Essex County Health Unit',
    'York Region Public Health Services'] )

# source: https://www12.statcan.gc.ca/census-recensement/2016/dp-pd/prof/search-recherche/lst/results-resultats.cfm?Lang=E&TABID=1&G=1&Geo1=&Code1=&Geo2=&Code2=&GEOCODE=35&type=0
meta['pop'].append( [113084,134943,102042,645862,202762,161977,109652,179083,548430,536917,161180,136093,193363,126638,169244,455526,
    447888,123820,76455,934243,1381744,138236,84201,535154,103593,540249,199840,196448,151884,33049,2731571,284461,398953,1109909] )

assert len(master[2]) == len(meta['pop'][2])
assert len(meta['names'][2]) == len(meta['pop'][2])


# type 3: US counties/states, John Hopkins
areas = dict()
junk_columns = ['UID', 'iso2', 'iso3', 'code3', 'FIPS', 'Admin2', 'Province_State','Country_Region', 'Lat', 'Long_']

# confirmed cases first
jh = pd.read_csv('https://github.com/CSSEGISandData/COVID-19/raw/master/csse_covid_19_data/' + \
        'csse_covid_19_time_series/time_series_covid19_confirmed_US.csv')
trans = jh.drop(junk_columns,axis=1).set_index(['Combined_Key']).T.reset_index()
epochs = np.array(pd.to_datetime(trans['index'], errors='coerce',format='%m/%d/%y').astype('int')) // 1000000000

for area in trans.columns[1:]:
    areas[area] = [to_int(epochs), to_int(trans[area]), [], [], [], [] ]

# deaths
jh = pd.read_csv('https://github.com/CSSEGISandData/COVID-19/raw/master/csse_covid_19_data/' + \
        'csse_covid_19_time_series/time_series_covid19_deaths_US.csv')
trans = jh.drop( junk_columns + ['Population'], axis=1).set_index(['Combined_Key']).T.reset_index()

for area in trans.columns[1:]:
    assert len(trans[area]) == len(areas[area][0]) 
    areas[area][2] = to_int(trans[area])

# population
trans = jh.drop( junk_columns, axis=1).set_index(['Combined_Key']).T.reset_index()
mask = trans['index'] == 'Population'
meta['pop'].append( to_int(trans[mask].iloc[0][1:]) )

del jh

# consolidate!
master.append( [] )
meta['names'].append( [] )
for area in areas.keys():
    master[-1].append( areas[area] )
    meta['names'][-1].append( area )

del areas

assert len(master[3]) == len(meta['pop'][3])
assert len(meta['names'][3]) == len(meta['pop'][3])


# type 4: World (John Hopkins minus Canada)
areas = dict()

# confirmed cases first
jh = pd.read_csv('https://github.com/CSSEGISandData/COVID-19/raw/master/csse_covid_19_data/' + \
        'csse_covid_19_time_series/time_series_covid19_confirmed_global.csv').sort_values(['Country/Region','Province/State'])
mask = jh['Country/Region'] != 'Canada'
trans = jh[mask].drop(['Lat','Long'],axis=1).set_index(['Province/State','Country/Region']).T.reset_index()
epochs = np.array(pd.to_datetime(trans['index'], errors='coerce',format='%m/%d/%y').astype('int')) // 1000000000

jh_names = [f"{y}" if type(x) == float else f"{x}, {y}" for x,y in trans.columns[1:]]

for area in trans.columns[1:]:
    areas[area] = [to_int(epochs), to_int(trans[area]), [], [], [], [] ]


meta['names'].append( [f"{y}" if type(x) == float else f"{x}, {y}" for x,y in trans.columns[1:]] )


# deaths
jh = pd.read_csv('https://github.com/CSSEGISandData/COVID-19/raw/master/csse_covid_19_data/' + \
        'csse_covid_19_time_series/time_series_covid19_deaths_global.csv').sort_values(['Country/Region','Province/State'])
mask = jh['Country/Region'] != 'Canada'
trans = jh[mask].drop(['Lat','Long'],axis=1).set_index(['Province/State','Country/Region']).T.reset_index()

for area in trans.columns[1:]:
    assert len(trans[area]) == len(areas[area][0]) 
    areas[area][2] = to_int(trans[area])

# recovered
jh = pd.read_csv('https://github.com/CSSEGISandData/COVID-19/raw/master/csse_covid_19_data/' + \
        'csse_covid_19_time_series/time_series_covid19_recovered_global.csv').sort_values(['Country/Region','Province/State'])
mask = jh['Country/Region'] != 'Canada'
trans = jh[mask].drop(['Lat','Long'],axis=1).set_index(['Province/State','Country/Region']).T.reset_index()

for area in trans.columns[1:]:
    assert len(trans[area]) == len(areas[area][0]) 
    areas[area][4] = to_int(trans[area])

del jh

# consolidate
master.append( [] )
for area in areas.keys():
    master[-1].append( areas[area] )

del areas

csse         = pd.read_csv('csse_population.csv',sep='\t').sort_values(['country','Province or territory'])
csse['name'] = [f"{d['country']}" if type(d['Province or territory']) == float else f"{d['Province or territory']}, {d['country']}" \
        for idx,d in csse.iterrows()]
csse_pop     = pd.merge( csse, pd.DataFrame({'name':jh_names}), how='right' ).fillna(0) # TODO: fix zeros in csse_population.csv!
meta['names'][-1]
meta['pop'].append( to_int(csse_pop['population']) )

assert len(master[4]) == len(meta['pop'][4])
assert len(meta['names'][4]) == len(meta['pop'][4])


# write it all out
with gzip.open(f'fitting_dataset.json.gz', 'wt', encoding="utf-8", compresslevel=9) as zipfile:
    json.dump( master, zipfile )

meta['data'] = master

with gzip.open(f'fitting_with_meta.json.gz', 'wt', encoding="utf-8", compresslevel=9) as zipfile:
    json.dump( meta, zipfile )
