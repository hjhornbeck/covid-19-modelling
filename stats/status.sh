#!/bin/sh

META_TIME=`ls -1 *_meta.json | while read ; do date +"%s" -r $REPLY ; done | sort -g | tail -1`
SERIES_TIME=`ls -1 *_series.json | while read ; do date +"%s" -r $REPLY ; done | sort -g | tail -1`

echo "[$META_TIME,$SERIES_TIME,82800]" > status.json
