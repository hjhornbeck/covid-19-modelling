#!/usr/bin/env python3

##### INCLUDES

import argparse
import gc
import gzip
import json
from multiprocessing import get_context, Manager, Pool, Process, Queue
import numpy as np
import os
import pandas as pd
import pickle
import queue
import sys
import time


##### VARIABLES


##### METHODS

def quality_file(file):
    return file.replace("posteriors","quality")

def reduced_file(file):
    return file.replace("posteriors","quality_reduced").replace(".gz","")

def print_mem( flush=True ):
    membytes = sys.getallocatedblocks() << 12
    if membytes < (1 << 30):
        print( f" [{membytes/(1<<20):.1f}MB", end='')
    else:
        print( f" [{membytes/(1<<30):.1f}GB", end='')
    print( f" {gc.get_count()}]", flush=flush )

def read_pickle_gz(file, verbose=False, delete=False):

    try:
      with gzip.open( file, 'rb' ) as zfile:
        output = pickle.load( zfile )
    except KeyboardInterrupt:
        return None
    except:
      if delete:
        os.remove( file )
      if verbose:
        t, v, tb = sys.exc_info()
        print( f'= Detected corrupt file {file}. Exception {t}, {str(v)}, line {tb.tb_lineno}.' )
      return None

    if verbose:
        print(f"= Finished loading {file}", end='')
        print_mem()

    return output


def status( args, dataset=None, file=None ):
    global metadata, subset, total

    if args.verbose and (file is not None):
        print(f"= Started loading {file} ({total - len(subset)}/{total})", end='')
        print_mem()

    if args.verbose and (dataset is not None):

        days, index = dataset
        kind, idx = index

        print(f"> {metadata['names'][kind][idx]} ({kind}/{idx}@{days})", end='')
        print_mem()


def inner_evaluate( blob ):

    # pull out the fit and associated quality
    fit, qual, args = blob
    posterior_sample = args.samples
    total_per_day = args.daytotal

    days, index = qual['index']
    kind, idx = index

    # we want this separated by the length of line
    day_totals   = [0] * len(qual['qual'])
    day_contents = [ [] for x in range(len(qual['qual'])) ]

    for start_idx,start in enumerate(qual['qual']):
        for idx in np.random.choice(len(qual['qual'][start_idx]), posterior_sample):
            for i,day in enumerate(qual['qual'][start_idx][idx]):
            
                if i >= len(day_totals):
                    # print( f"DEBUG: overflow for i={i}, idx={start_idx}, file={file}" )
                    day_totals.append( 0 )
                    day_contents.append( [] )
                day_totals[i] += 1
            
                # pull the associated fit
                row = [day, fit['pos'][start_idx][0], *fit['pos'][start_idx][3][idx]]

                # haven't filled up? Just append
                if len(day_contents[i]) < total_per_day:
                    day_contents[i].append( row )
                    
                # have filled up? Flip a coin to see if we add
                elif np.random.rand() < (total_per_day / day_totals[i]):
                    day_contents[i][np.random.choice(total_per_day)] = row

    # print( f"DEBUG: day totals = {day_totals}.", flush=True )

    del fit

    status( args, dataset=qual['index'] )
    return (qual['index'], day_contents, day_totals)


def outer_evaluate( fit_file, args, control ):

    # announce we've started
    if args.verbose:
        print( f"= Started to process {args.fitdir}/{fit_file}, pid {os.getpid()}.", flush=True )

    # load up the files
    qual_file = quality_file( fit_file )
    out_file  = reduced_file( fit_file )
    dataset   = list()

    status( args, file=fit_file )
    bundle_fit = read_pickle_gz( f"{args.fitdir}/{fit_file}", args.verbose, args.delete )

    status( args, file=qual_file )
    bundle_qual = read_pickle_gz( f"{args.qualdir}/{qual_file}", args.verbose, args.delete )

    # ensure the parameter sets match
    params   = bundle_fit[0]['params']
    for i,fit in enumerate(bundle_fit):
        if i == 0:
             continue
        assert fit['params'] == params

    for i,fit in enumerate(bundle_qual):
        assert fit['params'] == params

    # create a processing pool
    with Pool( processes=args.threads, maxtasksperchild=8 ) as pool:

        # launch some processes
        results = pool.imap_unordered( inner_evaluate, \
                [(bf,bundle_qual[i],args) for i,bf in enumerate(bundle_fit)], 4 )

        for result in results:
            meta, day_contents, day_totals = result
            for i,array in enumerate(day_contents):
                for v in array:
                    dataset.append( [meta[0], meta[1][0], meta[1][1], i + 1, *v] )

            # print stats
            status( args, dataset=meta )

    if args.verbose:
        print( f"= Finished processing {args.qualdir}/{qual_file}." )
    with open(f"{args.outdir}/{out_file}", 'wb') as file:
        pickle.dump( {'data':dataset, 'params':params}, file )
    if args.verbose:
        print( f"= Wrote output to {args.outdir}/{out_file}.", flush=True )
    control.put( fit_file )
    return None

##### MAIN

parser = argparse.ArgumentParser( description="Consolidate quality assessments and posteriors into a table.")
parser.add_argument( '-v',"--verbose", action='store_true', help="Give more verbose update messages." )

parser.add_argument( 'fitdir', metavar="FITDIR", default='generated/posteriors/0', \
                        help="The input directory containing fitting files." )
parser.add_argument( 'qualdir', metavar="QUALDIR", default='generated/quality/0', \
                        help="The input directory of matching fitting quality files." )
parser.add_argument( 'outdir', metavar="OUTDIR", default='generated/tables/0', \
                        help="The output directory of the resulting summary table." )

parser.add_argument( '-m',"--metadata", default="fitting_with_meta.json.gz", help="The dataset, with meta-data." )
parser.add_argument( '-n',"--samples", type=int, default=256, help="The number of random samples to draw from the posterior." )
parser.add_argument( '-d',"--daytotal", type=int, default=(1<<9), help="How many samples to keep for each day, at maximum." )

parser.add_argument( "--seed", type=int, default=17, help="The random number seed used. 0 = use the default generated one." )
parser.add_argument( '-T',"--threads", type=int, default=40, help="The number of threads to use. High numbers don't always help." )
parser.add_argument( '-f',"--files", type=int, default=6, help="The number of file pairs to keep in memory. Higher numbers = more memory." )
parser.add_argument( '-o',"--offset", type=int, default=0, help="The offset within the number of steps." )
parser.add_argument( '-s',"--stepsize", type=int, default=1, help="The number of steps to take." )
parser.add_argument( '-D',"--delete", action='store_true', help="Delete corrupted source files." )

args = parser.parse_args()


# tweak the thread count
if args.threads <= 0:
    args.threads = len(os.sched_getaffinity(0))

# set the seed, if asked
if args.seed != 0:
    np.random.seed( args.seed )

# load up the metadata
with gzip.open( args.metadata, 'rt', encoding="utf-8") as zfile:
    metadata = json.load( zfile )

dataset   = list()            # an iterable we can pass to pandas
control   = Queue()           # a way to signal completion
terminate = None              # storage for the process to terminate

params    = None              # enforce a consistent parameter set

todo = os.listdir( args.fitdir )       # the files to consider
np.random.shuffle( todo )              # scramble them to better load-balance

subset = todo[args.offset::args.stepsize]   # pull out a subset
total = len(subset)

proc_queue   = dict()     # processes to execute

                          # prevent status spamming
last_dataset_update = time.time()
update_times = 10


# while we have things to process
while (len(subset) > 0) or (len(proc_queue) > 0):

    # fill up the queue if there's input/space
    while (len(subset) > 0) and (len(proc_queue) < args.files):

        file = subset.pop()

        # append outer_evaluate to the queue
        temp = Process( target=outer_evaluate, args=(file,args,control) )
        temp.start()
        proc_queue[file] = temp
        if args.verbose:
            print( f"= Adding {args.fitdir}/{file} to the queue, pid {temp.pid}.", flush=True )

    # check if any processes have flagged themselves for termination
    try:
        terminate = control.get( timeout=0.01 )    # this will hang the parent process for a bit
        if args.verbose:
            print( f"= Received request to terminate process '{terminate}'.", flush=True )
    except queue.Empty:
        continue

    # one has, so end it
    if terminate is not None:
        if args.verbose:
            print( f"= About to join the process for '{terminate}'.", flush=True )
        proc_queue[terminate].join()
        if args.verbose:
            print( f"= Returned from process '{terminate}'.", flush=True )
        del proc_queue[terminate]             # free up space in the queue

        # incorporate its output into the dataset
        old_len = len(dataset)
        outfile = f"{args.outdir}/{reduced_file(terminate)}"
        if args.verbose:
            print( f"= Opening {outfile} to retrieve rows.", flush=True )

        with open( outfile, 'rb' ) as file:
            bundle = pickle.load( file )

        # enforce consistent parameters
        if params is None:
            params = bundle['params']
        else:
            assert params == bundle['params']
 
        dataset.extend( bundle['data'] )

        if args.verbose:
            print( f"= Added {len(dataset) - old_len} rows to the database.", flush=True )

        # no need for that output anymore, so save some space
        os.remove( outfile )

        terminate = None

print( f"= Converting {len(dataset)} rows to a pandas dataframe ...", end='', flush=True )
pred = pd.DataFrame( dataset, columns=['window size','type','series','day','mean(log(like))', 'n', 'm', 'b', 'log(σ)'] )
del dataset
print( " done.", flush=True )

# figure out the filename
filename = f"{args.outdir}/quality."
if 'preset' in params:
    filename += f"{params['preset']}"
else:
    filename += f"{params['clip']}.{params['fade']}.{params['type']}.{params['minlnstd']}"
filename += f".{params['dataset']}.{args.stepsize}.{args.offset}.pickle.gz"

print( f"= Writing {filename} ...", end='', flush=True )
with gzip.open(filename, 'wb', compresslevel=9) as zfile:
     pickle.dump( pred, zfile )
print( " done.", flush=True )

print( "= Finished.", flush=True )
