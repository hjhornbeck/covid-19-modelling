#!/usr/bin/env python3

##### NOTES

##### IMPORTS

import json
import re


##### VARIABLES

strip_name = re.compile( '.*<strong>([^<]+)</strong>.*' )


##### MAIN

# extract the name of each local geographic area
data = json.load( open('alberta.covid19stats.08.plotly.json','r') )
names = [strip_name.match( x ).group(1) for x in data['x']['calls'][2]['args'][6]]

meta_output = { 'names': names, 'cols': ['epoch','Cases','Active','Recovered','Death(s)'] }


# extract the geography / area / borders
meta_output['bounds'] = []
for i,geo in enumerate(data['x']['calls'][2]['args'][0]):
  meta_output['bounds'].append( [[ [[a,b] for a,b in zip(bord['lat'],bord['lng'])] for bord in area] for area in geo] )

# TODO: area and population

try:
    meta_old = json.load( open('data_meta.json',"r") )
except:
    meta_old = dict()

if meta_old != meta_output:
    json.dump( meta_output, open('data_meta.json','w'), allow_nan=False, separators=(',',':') )
