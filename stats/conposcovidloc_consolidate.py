#!/usr/bin/env python3

##### IMPORTS

import lzma
import json
import pandas as pd
import re
import sys


##### VARIABLES
find_date = re.compile( '.*conposcovidloc\.(\d+).csv.xz' )
junk_data = ['Accurate_Episode_Date', 'Age_Group', 'Client_Gender','Case_AcquisitionInfo', \
        'Outbreak_Related', 'Reporting_PHU_Address', 'Reporting_PHU_City', \
        'Reporting_PHU_Postal_Code','Reporting_PHU_Website', 'Reporting_PHU_Latitude', \
        'Reporting_PHU_Longitude']
phus = ['Algoma Public Health Unit', 'Brant County Health Unit', 
      'Chatham-Kent Health Unit', 'Durham Region Health Department',
      'Eastern Ontario Health Unit', 'Grey Bruce Health Unit',
      'Haldimand-Norfolk Health Unit',
      'Haliburton, Kawartha, Pine Ridge District Health Unit',
      'Halton Region Health Department', 'Hamilton Public Health Services',
      'Hastings and Prince Edward Counties Health Unit',
      'Huron Perth District Health Unit',
      'Kingston, Frontenac and Lennox & Addington Public Health',
      'Lambton Public Health',
      'Leeds, Grenville and Lanark District Health Unit',
      'Middlesex-London Health Unit',
      'Niagara Region Public Health Department',
      'North Bay Parry Sound District Health Unit',
      'Northwestern Health Unit', 'Ottawa Public Health',
      'Peel Public Health', 'Peterborough Public Health',
      'Porcupine Health Unit', 'Region of Waterloo, Public Health',
      'Renfrew County and District Health Unit',
      'Simcoe Muskoka District Health Unit', 'Southwestern Public Health',
      'Sudbury & District Health Unit', 'Thunder Bay District Health Unit',
      'Timiskaming Health Unit', 'Toronto Public Health',
      'Wellington-Dufferin-Guelph Public Health',
      'Windsor-Essex County Health Unit',
      'York Region Public Health Services']
      
##### MAIN

storage = {x:[[], [], [], []] for x in phus}

for file in sys.argv[1:]:
    
    match = find_date.match( file )
    if match:
        number = int( match.groups()[0] )
    else:
        print( f"ERROR: Couldn't extract the datetime from the filename '{file}'." )
        continue

    with lzma.open( file, 'rt' ) as lfile:
        try:
            dataset = pd.read_csv( lfile ).set_index(['Row_ID'])
        except:
            print( f"ERROR: Couldn't parse data from '{file}'." )
            continue

    for u in phus:
        storage[u][0].append( number )

    for col in junk_data:
        if col in dataset:
            dataset.drop([col], axis=1, inplace=True)

    tidy = dataset

    # cases
    temp = tidy.groupby(['Reporting_PHU']).count().T
    for u in phus:
        if u in temp:
            storage[u][1].append( int(temp[u][0]) )
        else:
            storage[u][1].append( -1 )

    # deaths
    temp = tidy[ tidy['Outcome1'] == 'Fatal' ].groupby(['Reporting_PHU']).count().T
    for u in phus:
        if u in temp:
            storage[u][2].append( int(temp[u][0]) )
        else:
            storage[u][2].append( -1 )

    # active
    temp = tidy[ tidy['Outcome1'] == 'Not Resolved' ].groupby(['Reporting_PHU']).count().T
    for u in phus:
        if u in temp:
            storage[u][3].append( int(temp[u][0]) )
        else:
            storage[u][3].append( -1 )

# convert the dictionary to a list
output = list()
for key in storage:
    output.append( storage[key] )

json.dump( output, open('ontario_series.json','w') ) # , allow_nan=False, separators=(',',':')
