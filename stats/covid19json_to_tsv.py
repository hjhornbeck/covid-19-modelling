#!/usr/bin/python3

import argparse
import json
import pandas as pd
import re
import sys


##### VARIABLES

get_number = re.compile( '.*stats\.(\d+)\.plotly.*' )
get_title  = re.compile( '<strong>([^<]+)</strong>(.*)' )
get_data   = re.compile( '(\d+) +([^<]+)' )
get_column = re.compile( '<th>([^ ][^<]+)</th>' )
get_rate   = re.compile( '([^<]+) *<br>Rate: *([^< ]+) *<br>Count: *(\d+) *' )
get_tests  = re.compile( 'Report Date: *([^< ]+)<br />Percent: *([^< ]+)<br />Number of tests: *([^< ]+)' )

##### METHODS

def extract_scatter( data, blank="tests performed", index='date' ):

    try:
        output = None
        for idx in range( len(data['x']['data']) ):

            if 'name' in data['x']['data'][idx]:
                colname = data['x']['data'][idx]['name']
            else:
                colname = blank

            temp = pd.DataFrame( data=zip(data['x']['data'][idx]['x'],data['x']['data'][idx]['y']), \
                columns=[index,colname] )
            temp.set_index( index, inplace=True )

            if idx == 0:
                output = temp
            else:
                output = pd.concat( [output,temp], axis=1, sort=True )
                output.index.name = index

        return output

    except:
        t, v, tb = sys.exc_info()
        print( f"extract_scatter: Exception {t}: {str(v)}, line {tb.tb_lineno}" )
        return None

def extract_tests( data, blank='Alberta' ):

    try:
        for idx in range( len(data['x']['data']) ):

            if 'name' in data['x']['data'][idx]:
                colname = data['x']['data'][idx]['name']
            else:
                colname = blank

            storage = list()
            for entry in data['x']['data'][idx]['text']:
                cols = get_tests.match( entry )
                if cols is None:
                    continue
                storage.append( [cols[1], cols[2], cols[3]] )

            if len(storage) > 0:
                temp = pd.DataFrame( data=storage, columns=['date','percent','number of tests'] )
                temp.set_index( 'date', inplace=True )

                if idx == 0:
                    output = temp
                else:
                    output = pd.concat( [output,temp], axis=1, sort=True )
                    output.index.name = index

        return output

    except:
        t, v, tb = sys.exc_info()
        print( f"extract_tests: Exception {t}: {str(v)}, line {tb.tb_lineno}" )
        return None

def extract_zones( data ):

    try:
        storage = list()
        for label in data['x']['data'][0]['text']:

            zone = get_rate.match( label )
            if zone is None:
                continue
            storage.append( [zone[1], zone[2], zone[3]] )

        temp = pd.DataFrame( data=storage, \
                columns=['Health Zone', 'Rate (per 100,000 population)', 'Count'] )
        temp.set_index( 'Health Zone', inplace=True )

        return temp

    except:
        t, v, tb = sys.exc_info()
        print( f"extract_zones: Exception {t}: {str(v)}, line {tb.tb_lineno}" )
        return None

def extract_map( data ):

    try:
        storage = dict()
        for ele in data['x']['calls']:

            # detect LGA-like data
            if ('args' not in ele) or (len(ele['args']) < 7):
                continue

            # we're not out of the woods yet, we need to verify it's LGA
            found_calgary = False
            for idx,label in enumerate( ele['args'][6] ):
                area = get_title.match( label )
                if area[1].lower() == 'city of calgary':
                    found_calgary = True
                    break

            # if Calgary was stored as a city, rather than broken up, it's not LGA
            if found_calgary:
                continue

            for idx,label in enumerate( ele['args'][6] ):

                area = get_title.match( label )
                if len(area.groups()) < 1:
                    continue
                temp = {'Local geographic area':area[1]}
                for count,cat in re.findall( get_data, area[2] ):
                    temp[cat.strip()] = count

                if idx == 0:
                    for key in temp:
                        storage[key] = [ temp[key] ]
                else:
                    for key in temp:
                        storage[key].append( temp[key] )
                
        output = pd.DataFrame( storage )
        return output.set_index( ['Local geographic area'] )

    except:
        t, v, tb = sys.exc_info()
        print( f"extract_map: Exception {t}: {str(v)}, line {tb.tb_lineno}" )
        return None

def extract_table( data ):

    if 'container' not in data['x']:
        return None

    header = [ 'index' ]
    for col in re.findall( get_column, data['x']['container'] ):
        header.append( col )

    if 'data' not in data['x']:
        return None

    storage = list()
    for idx,col in enumerate(data['x']['data']):
        storage.append( col )

    output = pd.DataFrame( data=zip(*storage), columns=header )
    output.set_index( 'index', inplace=True )

    return output


##### MAIN

# parse the command-line arguments
parser = argparse.ArgumentParser( description="convert a JSON pulled from covid19stats into a TSV file")
parser.add_argument( '-r',"--relaxed", action='store_true', help="Try all methods to extract data, not just the one we expect" )
parser.add_argument( 'files', metavar="NAME", nargs='+', help="The JSON files to parse" )
args = parser.parse_args()


for file in args.files:

    try:
        data = json.load( open(file,"r") )
        index = int( get_number.match( file )[1] )
    except:
        t, v, tb = sys.exc_info()
        print( f"load_data: Exception {t}: {str(v)}, line {tb.tb_lineno}" )
        continue

    output = None

    if args.relaxed:

        output = extract_table( data )
        if output is None:
            output = extract_map( data )
        if output is None:
            output = extract_tests( data )
        if output is None:
            output = extract_scatter( data )

    else:
        if index == 1:
            output = extract_scatter( data, index='health zone', blank='cases reported in the last week' )
        elif index == 2:
            output = extract_scatter( data, index='health zone', blank='active cases' )

        elif index == 3:
            output = extract_scatter( data, index='age group', blank='cases reported in the last week' )
        elif index == 4:
            output = extract_scatter( data, index='age group', blank='active cases' )

        elif index == 5:
            output = extract_scatter( data, index='route of suspected acquisition', blank='cases reported in the last week' )
        elif index == 6:
            output = extract_scatter( data, index='route of suspected acquisition', blank='active cases' )

        elif (index > 6) and (index <= 11):
            output = extract_scatter( data )

#        elif index == 8:
#            output = extract_zones( data )

        elif index == 12: 
            output = extract_scatter( data, index='case severity' )

        elif index == 13:
            output = extract_scatter( data )

        elif index == 14:
            output = extract_scatter( data, index='health zone', blank='Rate (per 100,000 population)' )

        elif index == 15:
            output = extract_scatter( data, index='health zone', blank='Rate (per 100,000 population)' )

        elif index == 16:
            output = extract_map( data )

        elif index == 17:
            output = extract_scatter( data, index='Date Reported to Alberta Health', blank='Test volume for COVID-19 (n)' )

        elif index == 18:
            output = extract_tests( data )

# TODO: extract test data from 19

        elif index == 20:
            output = extract_table( data )

        else:
            continue

    if output is not None:
        output.to_csv( file.replace('json','tsv'), sep='\t' )
