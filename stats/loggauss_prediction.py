#!/usr/bin/env python3

##### INCLUDES

from emcee_shared import *

import gzip
import json
from multiprocessing import Pool
import numpy as np
import time
import sys


##### METHODS

def filename( kind, idx ):
    return f'trendlines/quality/1/quality.1.0.x.{kind}.{idx}.json.gz'

def evaluate( combo ):

    quality = list()

    # figure out what our parameters are
    kind, idx = combo

    # clean up the series
    data = clean_series( dataset[kind][idx][0], dataset[kind][idx][1] )
    # deltas are very tough to fit with loggauss

    # load up the posterior
    with gzip.open(f'trendlines/posteriors/1/posteriors.1.0.x.{kind}.{idx}.json.gz', 'rt', encoding="utf-8") as zipfile:
        fits = json.load( zipfile )

    # for each saved fit
    for fit in fits['pos']:

        quality.append( [] )

        # determine the datapoints in the future
        i = fit[0]

        # evaluate those future datapoints, if possible
        if i == len(data[0]):
            continue

        future = [ (np.array(data[0][i:]) - data[0][0])/86400, data[1][i:] ]
        lg_model = LogGauss(future)

        for theta in fit[3]:

            # build up the log probabilities
            lp = lg_model.lnprior( theta )
            if not np.isfinite( lp ):
                quality[-1].append( [] )    # gotta stay in sync
                continue

            # build up the log probabilities
            llike = lg_model.lnlike( theta, consolidate=False )

            # convert to an average and save
            quality[-1].append(list( np.cumsum(llike) / np.arange(1,len(llike)+1) ))

    # all done
    return (quality, combo)

##### MAIN

# read off the index/step from the command line
try:
    idx  = int(sys.argv[1])
    step = int(sys.argv[2])
except:
    print("ERROR: Please supply an index and step size on the command line.")
    sys.exit(1)

# read in the datasets
with gzip.open( 'fitting_dataset.json.gz', 'rt', encoding="utf-8") as zipfile:
    dataset = json.load( zipfile )

# build up a to-do list
todo = list()
for kind_idx,kind in enumerate(dataset):
    for i,_ in enumerate(kind):
            todo.append( (kind_idx,i) )

# scramble it, deterministically
np.random.seed( 42 )
np.random.shuffle( todo )
np.random.seed()

# fire up multiprocessing
with Pool() as pool:

    for result in pool.imap_unordered( evaluate, todo[idx::step], 8 ):
#    for result in map( evaluate, todo[idx::step] ):

        quality, combo = result
        if (quality is None) or (len(quality) < 1):           # skip invalid results
            continue

        kind, idx = combo

        # gzip support, courtesy https://stackoverflow.com/questions/49534901/is-there-a-way-to-use-json-dump-with-gzip/49535758#49535758
        with gzip.open(f'trendlines/quality/1/quality.1.0.x.{kind}.{idx}.json.gz', 'wt', encoding="utf-8", compresslevel=9) as zipfile:
            json.dump( {'index':combo,'qual':quality}, zipfile )
        print( combo )
