#!/usr/bin/perl -w

# print out the header
print "id\tDate reported\tAlberta Health Services Zone\tGender\tAge Group\tCase Status\tCase type\n";

my @d;  # storage for the data

while(<>) {

	# alas, this has been changed AGAIN
	if( /,"data":\[(\[.*\])\],"container":"/ ) {

		for($1 =~ /\[("[^\]]+")\]/g) {
			my @temp = /"([^"]+)"/g;
			push @d, [@temp] if ($#temp > 100);
			}
		}
	elsif( /^\W+\["([^\]]+)"\]/ ) {

		my @temp = split '", "', $1;
		push @d, [@temp] if ($#temp > 100);
		}

	}

for ( my $i = 0;	$i < scalar(@{$d[0]});	$i++ ) {

	print "$_->[$i]\t"	for(@d);
	print "\n"
	}
