#!/usr/bin/env python3

##### IMPORTS

import json
import pandas as pd
import re
import sys


##### VARIABLES

find_date = re.compile( '.*alberta\.(\d+).*' )
output = dict()
meta = json.load( open('data_meta.json',"r") )

##### MAIN

# TODO: append province/country info to handle overlaps
lookup = { name:idx for idx,name in enumerate(meta['names']) }
output = [ [[], [], [], [], []] for x in meta['names'] ]
remap  = { "Athabasca":"Athabasca (Nearby Athabasca County, South Opportunity Md & East Lesser Slave Lake Md)",
           "Barrhead":"Barrhead (Barrhead County & Nw Woodlands County)",
           "Beaumont":"Beaumont (East Leduc County)",
           "Beaverlodge":"Beaverlodge (West Grande Prairie County)",
           "Black Diamond":"Black Diamond (Turner Valley & West Foothills Md)",
           "Bonnyville":"Bonnyville (Nearby Bonnyville Md)",
           "Boyle":"Boyle (Se Athabasca County)",
           "Calgary - Elbow":"Calgary - Elbow (& Nearby Neighbourhoods)",
           "Calgary - Fish Creek":"Calgary - Fish Creek (& Nearby Neighbourhoods)",
           "Calgary - Nosehill":"Calgary - Nose Hill (& Nearby Neighbourhoods)",
           "Calgary - Nose Hill":"Calgary - Nose Hill (& Nearby Neighbourhoods)",
           "Calgary - West Bow":"Calgary - West Bow (& Nearby Neighbourhoods)",
           "Canmore":"Canmore (Kananaskis - Bighorn Md - Stoney - West Rocky View County)",
           "Castor/Coronation/Consort":"Castor/Coronation/Consort (Special Area 4 & Paintearth County)",
           "Castor/Coronation/Consort (Special Area 4 & Painted Earth County)":"Castor/Coronation/Consort (Special Area 4 & Paintearth County)",
           "Chestermere":"Chestermere (Nearby Rocky View County)",
           "Claresholm":"Claresholm (Central Willow Creek Md)",
           "Cochrane-Springbank":"Cochrane - Springbank (Nearby Rocky View County)",
           "Cold Lake":"Cold Lake (East Bonnyville Md)",
           "Crossfield":"Crossfield (Nearby Rocky View County)",
           "Crowsnest Pass":"Crowsnest Pass (Ranchland Md)",
           "Cypress County":"Cypress County (Redcliff)",
           "Didsbury":"Didsbury (Carstairs & South Mountain View County)",
           "Drayton Valley":"Drayton Valley (Brazeau County & West Wetaskiwin County)",
           "Edmonton - Abbottsfield":"Edmonton - Abbottsfield (& Nearby Neighbourhoods)",
           "Edmonton - Bonnie Doon":"Edmonton - Bonnie Doon (& Nearby Neighbourhoods)",
           "Edmonton - Castle Downs":"Edmonton - Castle Downs (& Nearby Neighbourhoods)",
           "Edmonton - Duggan":"Edmonton - Duggan (& Nearby Neighbourhoods)",
           "Edmonton - Eastwood":"Edmonton - Eastwood (& Nearby Neighbourhoods)",
           "Edmonton - Jasper Place":"Edmonton - Jasper Place (& Nearby Neighbourhoods)",
           "Edmonton - Northgate":"Edmonton - Northgate (& Nearby Neighbourhoods)",
           "Edmonton - Rutherford":"Edmonton - Rutherford (& Nearby Neighbourhoods)",
           "Edmonton - Twin Brooks":"Edmonton - Twin Brooks (& Nearby Neighbourhoods)",
           "Edmonton - West Jasper Place":"Edmonton - West Jasper Place (& Nearby Neighbourhoods)",
           "Edmonton - Woodcroft East":"Edmonton - Woodcroft East (& Nearby Neighbourhoods)",
           "Edmonton - Woodcroft West":"Edmonton - Woodcroft West (& Nearby Neighbourhoods)",
           "Edson":"Edson (Central Yellowhead County)",
           "Fairview":"Fairview (Fairview Md & South Clear Hills Md)",
           "Falher":"Falher (Smoky River Md)",
           "Fort Macleod":"Fort Macleod (South Willow Creek Md)",
           "Fox Creek":"Fox Creek (Se Greenview Md)",
           "Frog Lake":"Frog Lake (West St. Paul County & South Bonnyville Md)",
           "Grande Cache":"Grande Cache (Sw Greenview Md)",
           "Grande Prairie County":"Grande Prairie County (Central And East Portions)",
           "High Level":"High Level (Mackenzie County & Nw Wood Buffalo)",
           "High Prairie":"High Prairie (Nearby Big Lakes County & Se Northern Sunrise County)",
           "High River":"High River (South Foothills Md & North Willow Creek Md)",
           "Hinton":"Hinton (West Yellowhead County)",
           "Innisfail":"Innisfail (South Red Deer County)",
           "Lac La Biche":"Lac La Biche (Lac La Biche County, Nw Smoky Lake County & Id 349)",
           "Lacombe":"Lacombe (& Nearby Lacombe County)",
           "Manning":"Manning (North Northern Lights County & North Clear Hills Md)",
           "Mayerthorpe":"Mayerthorpe (Lac Ste. Anne County)",
           "Newell":"Newell (Brooks & South Special Area 2)",
           "Okotoks-Priddis":"Okotoks-Priddis (North Foothills Md)",
           "Olds":"Olds (North Mountain View County)",
           "Oyen":"Oyen (Special Area 3, Acadia Md & East Special Area 2)",
           "Peace River":"Peace River (Peace Md, Northern Sunrise County & Sw Northern Lights County)",
           "Pincher Creek":"Pincher Creek (Pincher Creek County And Piikani)",
           "Planning & Special Area 2":"Planning & Special Area 2 (North Sa 2 & Hanna)",
           "Ponoka":"Ponoka (East Ponoka County)",
           "Red Deer County":"Red Deer County (Central And North Rd County & Parts Of Lacombe County)",
           "Red Deer - Sw":"Red Deer - Sw (Gaetz Ave)",
           "Rimbey":"Rimbey (West Ponoka County & Partial Lacombe County)",
           "Rocky Mountain House":"Rocky Mountain House (Clearwater County)",
           "Slave Lake":"Slave Lake (Nearby Lesser Slave County & Sw Opportunity Md)",
           "Smoky Lake":"Smoky Lake (Nearby Smoky Lake County & West Thorhild County)",
           "Spirit River":"Spirit River (Saddle Hills County, Spirit River Md & Birch Hills County)",
           "Starland County/Drumheller":"Starland County/Drumheller (& Se Kneehill County)",
           "St. Paul":"St. Paul (Saddle Lake And Surrounding St. Paul County)",
           "Strathmore":"Strathmore (Siksika, East Rocky View County & Wheatland County)",
           "Sundre":"Sundre (West Mountain View County)",
           "Swan Hills":"Swan Hills (South Big Lakes County)",
           "Tofield":"Tofield (Beaver County West)",
           "Thorsby":"Thorsby (West Leduc County)",
           "Valleyview":"Valleyview (Ne Greenview Md)",
           "Viking":"Viking (Beaver County East)",
           "Vulcan":"Vulcan (Vulcan County)",
           "Wabasca":"Wabasca (Nearby Opportunity Md)",
           "Westlock":"Westlock (Westlock County, West Thorhild County, South Lesser Slave River)",
           "Wetaskiwin County":"Wetaskiwin County (Maskwacis)",
           "Whitecourt":"Whitecourt (Sw Woodlands County)"
        }

for file in sys.argv[1:]:

    # we need the epoch seconds from the filename
    match = find_date.match( file )
    if match:
        number = int( match.groups()[0] )
    else:
        print( f"ERROR: Couldn't extract the datetime from the filename '{file}'." )
        continue

    try:
        db = pd.read_csv(file, sep='\t').fillna(-1)
    except:
        continue

    for idx,row in db.iterrows():

        # handle renamed LGAs first
        if row['Local geographic area'] in remap:
            i = lookup[ remap[row['Local geographic area']] ]

        elif row['Local geographic area'] == -1:   # KLUDGE: handle N/A's better
            if -1 in lookup:
                i = lookup[ -1 ]
            else:
                i = lookup[ "NA" ]

        elif row['Local geographic area'] not in lookup:
            print( f"ERROR: Could not find {row['Local geographic area']} in the meta!" )
            continue        # just drop it, and hope a human catches this error
        else:
            i = lookup[ row['Local geographic area'] ]

        output[ i ][0].append( number )
        if 'Cases' in row:
            output[ i ][1].append( row['Cases'] )
        elif 'case(s)' in row:
            output[ i ][1].append( row['case(s)'] )
        else:
            output[ i ][1].append( -1 )

        if 'Active' in row:
            output[ i ][2].append( row['Active'] )
        else:
            output[ i ][2].append( -1 )

        if 'Recovered' in row:
            output[ i ][3].append( row['Recovered'] )
        else:
            output[ i ][3].append( -1 )

        if 'Death(s)' in row:
            output[ i ][4].append( row['Death(s)'] )
        else:
            output[ i ][4].append( -1 )


json.dump( output, open('data_series.json','w'), allow_nan=False, separators=(',',':') )
