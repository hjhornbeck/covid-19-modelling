#!/usr/bin/env python3

import argparse
import gzip
import json
import lzma
from multiprocessing import Pool
import os
import pickle
import sys



parser = argparse.ArgumentParser( description="convert files in .json.gz format to .pickle.xz format.")
parser.add_argument( '-d',"--delete", action='store_true', help="Delete the original files once converted." )
parser.add_argument( '-v',"--verbose", action='store_true', help="Print the current status of the conversion." )
parser.add_argument( '-T',"--threads", type=int, default=4, help="Number of threads to use. <= 0 is all." )
parser.add_argument( 'indir', metavar="INPUTDIR", help="The input directory to read from." )
parser.add_argument( 'outdir', metavar="OUTPUTDIR", help="The output directory to save files to." )
args = parser.parse_args()

threads = args.threads
if threads <= 0:
   threads = None

files  = os.listdir(args.indir)
count  = 0
indir  = args.indir
outdir = args.outdir

def evaluate(file):
    global indir, outdir

    if not file.endswith('.json.gz'):
        return None

    newfile = file.replace('.json.gz', '.pickle.xz')

    with gzip.open( f"{indir}/{file}", 'rt' ) as zfile:
        dataset = json.load( zfile )

    with lzma.open( f"{outdir}/{newfile}", 'wb', check=lzma.CHECK_SHA256, preset=9 ) as xfile:
        pickle.dump( dataset, xfile )

    return (file,newfile)

with Pool(threads) as pool:

    for result in pool.imap_unordered( evaluate, files, 1 ):

      count += 1
      if result is None:
           continue

      file, newfile = result

      if args.delete:
        os.remove( f"{args.indir}/{file}" )

      if args.verbose:
        print( f"{count: 6d}/{len(files): 6d}: {file} -> {newfile}", flush=True )
