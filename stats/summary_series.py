#!/usr/bin/env python3

import datetime as dt
import json
import pandas as pd

data = pd.read_csv( 'canada.outbreak_update.csv' ).fillna( -1 )

# based on https://stackoverflow.com/questions/35630098/convert-a-column-of-datetimes-to-epoch-in-python
data['epoch'] = pd.to_datetime(data['date'], format="%d-%m-%Y").astype('int') // int(1e9)

provinces = pd.Categorical( data['prname'] ).categories.values
output = dict()

for prov in provinces:

    mask = (data['prname'] == prov)
    output[prov] = [ list(data['epoch'][mask].astype('int')) ]
    for x in ['numtotal','numrecover','numdeaths','numtested']:
        output[prov].append( list(data[x][mask].astype('float')) )

json.dump( output, open('summary_series.json','w'), allow_nan=False, separators=(',',':') )

