// layout-specific global vars

const layout_storage = 'layout_state';

const map_layout     = { type: 'component', componentName: 'map', title: 'Overview Map', componentState: {} };
const help_layout    = { type: 'component', height: 25, componentName: 'help', title: 'Welcome / Help', componentState: {} };
const preview_layout = { type: 'component', componentName: 'preview', title: 'Preview', componentState: {} };
const lga_template   = { type: 'component', componentName: 'lga', title: 'CHANGE THIS', componentState: { name: 'LCA' } };
const summary_layout = { type: 'component', height: 35, componentName: 'summary', title: 'Summary', componentState: {} };
const predict_layout = { type: 'component', height: 45, componentName: 'predictions', title: 'Predictions', componentState: {} };

// the three pre-defined layouts
const default_layout_config = { 
	settings: { showPopoutIcon: false }, 
	content: [	{type: 'row', content:[
				{type: 'column', width: 33, content:[map_layout, help_layout]},
				{type: 'column', content:[preview_layout, predict_layout, summary_layout]}
			]}
        	]
	};

const default_layout_config_compact = { 
	settings: { showPopoutIcon: false }, 
	content: [	{type: 'row', content:[
				{type: 'column', width: 33, content:[map_layout, help_layout]},
				{type: 'column', content:[preview_layout, summary_layout]}
			]}
        	]
	};

const default_layout_config_mobile = { 
	settings: { showPopoutIcon: false },
	"dimensions":{"borderWidth":10,"minItemHeight":20,"minItemWidth":20,"headerHeight":40},
	"content":[	{"type":"row", "content":[
 		{"type":"stack", "width":33,"content":[ help_layout, map_layout ]},
		{"type":"stack", "content":[ preview_layout, summary_layout ]}
		]} 
	]
};

// we need an external object to track the preview
class PreviewManager {

	constructor() {

		// use this to track the last added preview box
		// format: [HTML id, component, state]
		this._stack = new Array();
		}

	// a static method to help deal with title changes
	static set_title( container, state ) {

		// first off, could we?
		if( (dataset.data_meta == null) || !('lga' in state) ) {

			container.setTitle( "Preview" );
			return;
			}

		// if yes, then do so
		var title = dataset.data_meta['names'][ state['lga'] ];
		if( 'series' in state )
			title += ": " + data_series_names[ state['series'] ];

		container.setTitle( title );
		}

	// when new preview windows pop up ...
	register( id, component, state ) { 

		// on second thought, let's protect against doubles
		for( var i = 0; i < this._stack.length; i++ )
			if( this._stack[i][0] == id )
				return false;

		this._stack.push( [id,component,state] );
		return true;
		}

	// ... or are destroyed
	unregister( id ) {

		var found = false;
		for( var i = 0; i < this._stack.length; i++ )
			if( this._stack[i][0] == id ) {

				this._stack.splice( i, 1 );
				found = true;
				}

		return found;
		}

	// finally, something to handle clicking
	click( district ) {

		// first off, is there anything to do?
		if( this._stack.length == 0 )
			return;

		// otherwise, update the state
		var idx = (this._stack.length - 1);
		this._stack[idx][2]['lga'] = district ;
		this._stack[idx][1].extendState( { lga: district } );

		// no series selected? Go with the default
		if( !('series' in this._stack[idx][2]) ) {

			this._stack[idx][2]['series'] = 0;
			this._stack[idx][1].extendState( { series: 0 } );
			}

		// ensure we've got the HTML correctly set up
		var ele = document.getElementById( this._stack[idx][0] );
		if( ! ele )
			setup_preview( this._stack[idx][0], this._stack[idx][1], this._stack[idx][2] );

		// and trigger a redraw
		redraw_preview( this._stack[idx][2] );
		PreviewManager.set_title( this._stack[idx][1], this._stack[idx][2] );

		}

	} // PreviewManager

var previewManager = new PreviewManager();	// plus an instance we can access


var layout; 		// the current layout

var saved_layout = localStorage.getItem( layout_storage );
if( saved_layout !== null )

	layout = new GoldenLayout( JSON.parse( saved_layout ) );

else {

	// choose a layout based on the window's height
	if( isMobile || (window.innerHeight <= 600) )
		layout = new GoldenLayout( default_layout_config_mobile, '#'+layout_element );
	else if( window.innerHeight <= 800 )
		layout = new GoldenLayout( default_layout_config_compact, '#'+layout_element );
	else
		layout = new GoldenLayout( default_layout_config, '#'+layout_element );

	// TODO: a proper helper to fix the layout after a reset
	height_hack();
	}


// register components and link their drag sources
map_component = function( container, componentState ) {
	
	// save these for later
	this._container = container;
	this._state = componentState;

	// find an unoccupied ID
	if( !componentState['index'] ) { // imperfect, but it seems to work better
		componentState['index'] = Math.round(Math.random() * 1000000);
		container.extendState( { index: componentState['index'] } );
		}

	var name = map_html_target( componentState['index'] );

	// load ourselves at that ID, but only when the HTML element is available
	container.getElement().html( '<div class="overview_map" id="' + name + '"></div>' );

	// TODO: store coords and zoom in state?
	
	// load it up
	async function load_map() {

		var ele = wait_until_visible( name );
		await sleep(waiting_for_setup);	// Leaflet needs some extra time
		await redraw_map( name, componentState );
		}
	load_map();

	container.on('resize', function(){ redraw_map(name, componentState); } );
	}
layout.registerComponent( 'map', map_component );

var element = document.getElementById('overview_button_menu');
layout.createDragSource( element, map_layout );

// SUMMARY: summaries of the stats in various areas
summary_component = function( container, componentState ) {

	// save these for later?
	this._container = container;
	this._state = componentState;

	// find an unoccupied ID (ugly, but it'll work well enough)
	if( !componentState['index'] ) {
		componentState['index'] = Math.round(Math.random() * 10000000);
		container.extendState( { index: componentState['index'] } );
		}

	var name = summary_html_target( componentState['index'] ); 

	// generate some defaults
	if( !componentState['series'] ) {
		componentState['series'] = 0;
		container.extendState( { series: componentState['series'] } );
		}

	if( !componentState['region'] ) {
		componentState['region'] = "Alberta";
		container.extendState( { series: componentState['region'] } );
		}

	// we need a helper function to change the title
	function title_text( state ) { 

		// handle null values, which sometimes pop up after reset
		var ser = 0;
		if( ('series' in state) && (state['series'] >= 0) )
			ser = state['series'];

		var reg = 'Alberta';
		if( ('region' in state) && state['region'] )
			reg = state['region'];

		return reg + ": " + summary_series_names[ser];
		}

	container.setTitle( title_text(componentState) );

	// next up, the options (gotta respect the stored state!)
	var series = '';
	for( var i = 0; i < summary_series_names.length; i++ ) {

		series += '<option value="' + i + '"';
		if( componentState['series'] == i )
			series += ' selected';
		series += '>' + summary_series_names[i] + '</option>';

		}

	// set up a skeleton at that ID
	container.getElement().html( '<span class="summary">' + 
		'<span class="summary" id="' + name + '"></span>' + 
		'<span class="summary_selector"><select id="region">' +
		'<option>Alberta</option>' +
		'</select><select id="series">' + 
		series +
		'</select></span>' +
		'</span>' );

	// ensure we adjust ourselves on resize
	container.on('resize', function(){ redraw_summary(componentState,true); } );

	// launch the defered update of the drop-down menus
	async function summary_dropdown_update() {

		var ele = await wait_until_visible( name );
		if( !ele ) {
			console.log( "ERROR: Could not find '" + name + "'" );
			return;
			}

		await dataset.wait_on_summary_series();
		if( dataset.summary_series == null ) {
			console.log("ERROR: '" + name + "' could not find the dataset.");
			return;
			}

		// build up the option list and apply it
		var regions = '';
		for( var region in dataset.summary_series ) {
			regions += '<option';
			if( componentState['region'] == region )
				regions += ' selected';
			regions += '>' + region + '</option>';
			}

		$( '#'+name ).parent().find( '#region' ).html( regions );

		// now then, to update the onchange events
		$( '#'+name ).parent().find( '#series' ).get()[0].onchange = function(thing) {

			componentState['series'] = parseInt(thing.target.value);
			container.extendState({ series: componentState['series'] });
			redraw_summary( componentState );
			container.setTitle( title_text(componentState) );
			}

		$( '#'+name ).parent().find( '#region' ).get()[0].onchange = function(thing) {

			componentState['region'] = thing.target.value;
			container.extendState({ region: thing.target.value });
			redraw_summary( componentState );
			container.setTitle( title_text(componentState) );
			}

		}
	summary_dropdown_update();

	}
layout.registerComponent( 'summary', summary_component );

element = document.getElementById('summary_button_menu');
layout.createDragSource( element, summary_layout );


// PREDICTIONS: a quick way to sample various predictions. Needs refactoring.
predict_component = function( container, componentState ) {

	// save these for later
	this._container = container;
	this._state = componentState;

	// find an unoccupied ID
	if( !this._state['index'] )
		this._state['index'] = 1;

	var name = predict_html_target( this._state['index'] );
	var ele = document.getElementById( name );
	while ( ele ) {

		this._state['index'] += 1;
		name = predict_html_target( this._state['index'] );
		ele = document.getElementById( name );
		}

	// load ourselves at that ID, but only when the HTML element is available
	container.getElement().html( '<span class="predict" id="' + name + '"></span>' );

	// ensure we adjust ourselves on resize
	container.on('resize', function(){ redraw_predict(componentState,true); } );

	}
layout.registerComponent( 'predictions', predict_component );

element = document.getElementById('predict_button_menu');
layout.createDragSource( element, predict_layout );


// PREVIEW: click to freeze the last one added
preview_component = function( container, componentState ) {

	// find an unoccupied ID
	if( !componentState['index'] ) {
		componentState['index'] = Math.round(Math.random() * 10000000);
		container.extendState( { index: componentState['index'] } );
		}

	var name = preview_html_target( componentState['index'] ); 

	// no defaults! We want this inert before we fill it up
	
	// the title needs to be set asynchroniously, as the dataset may not be ready
	async function set_title() { 

		await dataset.wait_on_data_meta();
		PreviewManager.set_title( container, componentState );
		}
	set_title();

	// set up a skeleton at that ID, but only if necessary
	if( !('lga' in componentState) )
		container.getElement().html( '<center><h2>Please click a district on the map.</h2></center>' );
	else
		setup_preview( name, container, componentState );

	// ensure we adjust ourselves on resize, and drop out of updates on destruction
	container.on('resize', function(){ redraw_preview(componentState,true); } );
	container.on('destroy', function(){ previewManager.unregister( name ); } );

	// finally, register ourselves
	previewManager.register( name, container, componentState );

	}

// use this to set up the preview's HTML
async function setup_preview( name, container, componentState ) {

	// need this to be in place first
	await dataset.wait_on_data_meta();

	var ele = document.getElementById( name );
	if( !ele ) {

		// no HTML means we've got to install it ourselves
		var series = '';
		for( var i = 0; i < data_series_names.length; i++ ) {

			series += '<option value="' + i + '"';
			if( componentState['series'] == i )
				series += ' selected';
			series += '>' + data_series_names[i] + '</option>';

			}

		container.getElement().html( '<span class="preview">' + 
			'<span class="preview" id="' + name + '"></span>' + 
			'<span class="preview_selector"><select id="series">' + 
			series +
			'</select></span>' +
			'</span>' );

		}

	// give the HTML time to take effect
	ele = await wait_until_visible( name );
	if( !ele ) {
		console.log( "ERROR: Could not find '" + name + "'" );
		return;
		}

	await dataset.wait_on_data_meta();
	if( dataset.data_meta == null ) {
		console.log("ERROR: '" + name + "' could not find the dataset.");
		return;
		}

	// update the onchange events, once possible
	if( 'lga' in componentState )
		$( '#'+name ).parent().find( '#series' ).get()[0].onchange = function(thing) {

			componentState['series'] = parseInt(thing.target.value);
			container.extendState({ series: componentState['series'] });
			redraw_preview( componentState );
			container.setTitle( PreviewManager.set_title(container, componentState) );
			}
	
	} // setup_preview

layout.registerComponent( 'preview', preview_component );

element = document.getElementById('preview_button_menu');
layout.createDragSource( element, preview_layout );


// HELP: A boring ol' Welcome/Help panel
layout.registerComponent( 'help', function( container, componentState ){

	// set up our contents
	container.getElement().html( $( '#help_template' ).html() );

	// TODO: change the IDs around

	// install the drag-and-drop handlers
	async function install_help_dnd() { 

		// pause for a bit
		await sleep(1000);

		// might as well do them all, I'm feeling lazy
		var elements = document.querySelectorAll('#overview_button_help');
		elements.forEach( function(e) { layout.createDragSource( e, map_layout ); } );

		elements = document.querySelectorAll('#summary_button_help');
		elements.forEach( function(e) { layout.createDragSource( e, summary_layout ); } );

		elements = document.querySelectorAll('#predict_button_help');
		elements.forEach( function(e) { layout.createDragSource( e, predict_layout ); } );

		elements = document.querySelectorAll('#preview_button_help');
		elements.forEach( function(e) { layout.createDragSource( e, preview_layout ); } );

		elements = document.querySelectorAll('#help_button_help');
		elements.forEach( function(e) { layout.createDragSource( e, help_layout ); } );

		};

	install_help_dnd();
});

element = document.getElementById('help_button_menu');
layout.createDragSource( element, help_layout );



// save any changes to the layout
layout.on( 'stateChanged', function(){
    var state = JSON.stringify( layout.toConfig() );
//    console.log( "DEBUG: state = " + state );
    localStorage.setItem( layout_storage, state );
});



// a helper function to reset the layout
function reset_layout() {

	localStorage.removeItem( layout_storage );
	location.reload();
	}

// an inelegant way to fix the layout after a reset
async function height_hack() { 

	await sleep();

	var saved_layout = localStorage.getItem( layout_storage );
	while( !saved_layout ) {
			
		await sleep();
		saved_layout = localStorage.getItem( layout_storage );
		}
			
	location.reload();
	}


// hide the top menu after a spell
var fade_menu_bar = true;
var fade_bar_color = false;
async function hide_menu_bar( delay=3000 ) { 

	fade_menu_bar = true;
	if( delay > 0 )
		await sleep(delay);

	// allow sloppy mouse movement to cancel this
	if( fade_menu_bar )
		$( "#top_menu" ).fadeOut(1000);
	}

async function show_menu_bar( delay=0 ) { 

	fade_menu_bar = false;

	// de-emphasize the menu's colour, after use
	if( !fade_bar_color ) {

		document.getElementById("menu_container").style.backgroundColor = 
			map_colours[map_colours.length >> 2];
		fade_bar_color = true;
		}

	if( delay > 0 )
		await sleep(delay);

	$( "#top_menu" ).fadeIn(150);
	}

// inject these events
async function inject_menu_js() {

	var ele = document.getElementById("menu_container");
	while( ! ele ) {

		await sleep();
		ele = document.getElementById("menu_container");
		}

	// ensure the delay parameter sticks
	ele.onmouseleave = async function(){ hide_menu_bar(3000); };
	ele.onmouseenter = show_menu_bar;
	ele.onmousedown  = show_menu_bar;
	ele.onmouseup    = async function(){ hide_menu_bar(3000); };
	}
inject_menu_js();

// trigger the menu bar to disappear after load
var old_onload = window.onload;
window.onload = function() {

	if( old_onload )
		old_onload();
	hide_menu_bar();
	}

// actually initialize the layout
layout.init();

