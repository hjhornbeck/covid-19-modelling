
// load up the datasets
class Datasets {

	// all that we need for a dataset
	constructor(update, data_meta, data_series, summary_meta, summary_series) {

		// allow for implementation flexibility
		this._checkURL = update;
		this._dmURL = data_meta;
		this._dsURL = data_series;
		this._smURL = summary_meta;
		this._ssURL = summary_series;

		// store the update times
		this._m_date = 0;
		this._s_date = 0;
		this._s_next = 0;
		this._interv = 15*60;	// every 15 minutes

		// make some room for the datasets themselves
		this._data_meta       = null;
		this._data_series     = null;
		this._data_derived    = null;
		this._summary_meta    = null;
		this._summary_series  = null;
		this._summary_derived = null;

		// track what's happening behind the scenes
		this._periodic   = false;
		this._checkhttp  = null;

		}
		
	// some getters, to make certain things easier
	get data_meta() {  return this._data_meta; }
	get data_series() {  return this._data_series; }
	get data_derived() {  return this._data_derived; }
	get summary_meta() {  return this._summary_meta; }
	get summary_series() {  return this._summary_series; }
	get summary_derived() {  return this._summary_derived; }
	
	get series_updated_on() { return this._s_date; }
	get series_next_update() { return this._s_next; }
	get meta_updated_on() { return this._m_date; }
	get update_interval() { return this._interv; }
	get periodic_active() { return this._periodic; }
	get updating() { return this._checkhttp != null; }

	// functions for cleaning/modifying data
	static eliminate_NaNs( ) {

		var retVal = [];

		if( arguments.length == 1 ) {
			for( var it = 0; it < arguments[0].length; it++ )

				if( arguments[0][it] >= 0 )
					retVal[it] = arguments[0][it];

			}
		else if ( arguments.length == 2 ) {

			var x = arguments[0];
			var y = arguments[1];

			// transpose and filter
			var paired   = x.map( function(v,i){ return [v,y[i]]} );
			var filtered = paired.filter( function(v) { 
				return (typeof(v[0]) != 'undefined') && (typeof(v[1]) != 'undefined'); } );

			retVal[0] = new Array();
			retVal[1] = new Array();

			// restore to a convenient format
			for(var i = 0; i < filtered.length; i++) { 
				retVal[0].push( filtered[i][0] ); 
				retVal[1].push( filtered[i][1] ); 
				}

			}

		return retVal;
		}

	static remove_repeats( x, y, threshold=86400 ) {

		var y_n = [];
		var last_it = -1;	// gotta inchworm this

		for( var it = 0; it < x.length; it++ ) {

			if( (typeof(y[it]) == 'undefined') || (typeof(x[it]) == 'undefined') )
				continue;

			// no prior value, or the new value changed, or too much time has elapsed
			if( (last_it < 0) || (y[it] != y[last_it]) || ((x[it] - x[last_it]) >= threshold) ) {

				y_n[it] = y[it];
				last_it = it;
				}
			}

		/* ensure the lengths match
		if( y_n.length < x.length )
			y_n[ x.length-1 ] = null;
		*/
		return y_n;
		}

	static delta( y ) {

		var retVal = [];
		var last_it = -1;		// do the last valid entry
		for( var it = 0; it < y.length; it++ )

			if( typeof(y[it]) != 'undefined' ) {

				if( last_it < 0 )
					last_it = it;
				else {
					retVal[it] = y[it] - y[last_it];
					last_it = it;
					}
				}

		return retVal;
		}

	static offset( x, amount, scale=1 ) {

		var retVal = [];
		for( var it = 0; it < x.length; it++ )
			retVal[it] = x[it]*scale + amount;

		return retVal;
		}

	static sum( x, y ) {

		var retVal = [];
		for( var it = 0; it < x.length; it++ )

			if( (typeof(x[it]) != 'undefined') && (typeof(y[it]) != 'undefined') )
				retVal[it] = x[it] + y[it];

		return retVal;
		}

	static pair_divide( numer, denom ) {

		var retVal = [];
		for( var it = 0; it < denom.length; it++ )

			if( (typeof(numer[it]) != 'undefined') && (denom[it] > 0) )
				retVal[it] = numer[it] / denom[it];

		return retVal;
		}


	// calculate the derived datasets
	_summary_calculate() {

		// ensure the datasets are in place
		if( (this._summary_meta == null) || (this._summary_series == null) )
			return;

		// swap in the calculated in one fell swoop
		var temp	= new Object();
		var tzOff	= (new Date()).getTimezoneOffset()*60000;

		// for each summary area
		for( var idx in dataset.summary_series ) {

			// cache a few values
			var cache = new Array();
			for( var i = 0; i < 5; i++ )	// 'epoch','numtotal','numrecover','numdeaths','numtested'
				cache[i] = Datasets.eliminate_NaNs( dataset.summary_series[idx][i] );

			cache[5] = Datasets.sum( cache[2], cache[3] );	// recovered + deaths
			cache[6] = Datasets.delta( cache[1] );		// delta( confirmed + probable )
			cache[7] = Datasets.delta( cache[4] );		// delta( tested )

			temp[idx] = [

				// 0: the datetime, with an offset to re-align the data
				Datasets.offset( cache[0], tzOff, 1000 ),

				// 1: daily new/confirmed cases
				cache[6],

				// 2: daily confirmed deaths
				Datasets.delta( cache[3] ),

				// 3: deaths + recovered / cases
				Datasets.pair_divide( cache[5], cache[1] ), 

				// 4: daily cases / daily tested
				Datasets.pair_divide( cache[6], cache[7] )

				];

			} // for( each summary area )

		// swap the new value in
		this._summary_derived = temp;

		}

	_data_calculate() {

		// ensure the datasets are in place
		if( (this._data_meta == null) || (this._data_series == null) )
			return;

		// swap in the calculated in one fell swoop
		var temp	= { series: new Object(), meta: { hist_mean: [], max_mean: 0 } };
		var tzOff	= 0; // (new Date()).getTimezoneOffset()*60000;
		var now		= (new Date()).getTime();

		var max_mean	= 0;
		const case_span = 7*86400 * 1000;    // 1 week, in milliseconds

		// for each data area
		for( var idx in dataset.data_series ) {

			// cache a few values
			var cache = new Array();
			for( var i = 0; i < dataset.data_meta.cols.length; i++ )
							// "epoch", "Cases", "Active", "Recovered", "Death(s)"
				if( i == 0 )
					cache[i] = Datasets.eliminate_NaNs( dataset.data_series[idx][i] );
				else
					cache[i] = Datasets.remove_repeats( cache[0],
						Datasets.eliminate_NaNs( dataset.data_series[idx][i] ) );

			cache[5] = Datasets.sum( cache[3], cache[4] );		// recovered + deaths
			cache[6] = Datasets.delta( cache[1] );			// delta( confirmed + probable )
			cache[7] = Datasets.offset( cache[0], tzOff, 1000 );	// offset and scaled times

			temp['series'][idx] = [

				// 0: daily new/confirmed cases
				Datasets.eliminate_NaNs( cache[7], cache[6] ),

				// 1: daily confirmed deaths
				Datasets.eliminate_NaNs( cache[7], Datasets.delta( cache[4] ) ),

				// 2: deaths + recovered / cases
				Datasets.eliminate_NaNs( cache[7], Datasets.pair_divide(cache[5], cache[1]) )

				];

			// now figure out the mean
			var shortcut = temp['series'][idx][0];
			var i = shortcut[0].length - 1;
			var mean = 0;

			// while we're within our scope
			while( (i > 0) && (now - shortcut[0][i]) < case_span ) {

				mean += shortcut[1][i];
				i--;	// TODO: this overshoots, which may not be valid
				}
			
			// still at zero? check if we're still zero over two weeks
			if( mean == 0 ) {

				// inflate the value slightly to flag it
				mean = 0.00001;
				var j = i;
				while( (j > 0) && (shortcut[1][j] <= 0) )
					if ((now - shortcut[0][j]) > 14*86400*100) {
						// we were, re-flag and bail
						mean = 0;
						break;
						}
					else
						j--;
				}

			if( i < (shortcut[0].length - 1) )
				mean *= 86400*1000 / (shortcut[0][shortcut[0].length - 1] - shortcut[0][i+1]);

			if( mean > max_mean )
				max_mean = mean;

			temp['meta']['hist_mean'][idx] = mean;

			} // for( each data area )

		// don't forget to save the max
		temp['meta']['max_mean'] = max_mean;

		// swap the new value in
		this._data_derived = temp;

		}

	// update the datasets
	async update() {

		// don't overlap with ourselves
		const now = (new Date()).getTime() * 0.001;
		if( (now < this._s_next) || this._checkhttp )
			return;

		// inspired by: https://gist.github.com/msmfsd/fca50ab095b795eb39739e8c4357a808
		this._checkhttp = await fetch( this._checkURL );
		
		if( !this._checkhttp.ok ) {

			this._checkhttp = null;
			return;
			}

		let stat = await this._checkhttp.json();

		// first check if the metadata needs updating
		if( stat[0] > this._m_date ) {

			if( this._smURL ) {
				this._checkhttp = await fetch( this._smURL );
				if( this._checkhttp.ok )
					this._summary_meta = await this._checkhttp.json();
				}

			if( this._dmURL ) {
				this._checkhttp = await fetch( this._dmURL );
				if( this._checkhttp.ok )
					this._data_meta = await this._checkhttp.json();
				}

			this._m_date = stat[0];
			}
		
		// now for the series data
		if( stat[1] > this._s_date ) {

			if( this._ssURL ) {
				this._checkhttp = await fetch( this._ssURL );
				if( this._checkhttp.ok )
					this._summary_series = await this._checkhttp.json();
				}

			if( this._dsURL ) {
				this._checkhttp = await fetch( this._dsURL );
				if( this._checkhttp.ok )
					this._data_series = await this._checkhttp.json();
				}

			this._s_date = stat[1];
			this._s_next = stat[1] + stat[2];
			}

		// finally, the calculated datasets
		this._summary_calculate();
		this._data_calculate();

		// tidy up
		this._checkhttp = null;
		
		} // update()

	// toggle periodic updates of the datasets. true = start, false = stop.
	periodic_update( active = true ) {

		// just flag if asked
		if( !active ) {
			this._periodic = false;
			return;
			}

		// otherwise, create an anonymous function that does the deed
		async function do_update( ref ) {

			// while we're doing periodic updates
			while( ref.periodic_active ) {

				var now = (new Date()).getTime() * 0.001;
				var wait = ref.series_next_update - now;
				if( wait > 0 ) {

					await sleep( (wait + Math.random()*ref.update_interval)*1000 );
					continue;	// catch any status changes
					}
				
				await ref.update();
				await sleep( ref.update_interval*1000 );

				}

			}

		// ensure we don't launch more than one copy
		if( !this._periodic ) {

			this._periodic = true;
			do_update( this );
			}

		} // periodic_update

	// wait until some values switch to non-null
	async wait_on_summary_series( timeout = 60*1000 ) {

		var count = timeout / busy_loop;
		while( (this._summary_series == null) && (count > 0)  ) {

			count--;
			await sleep(busy_loop);
			}

		if( count <= 0 )
			return null;
		else
			return this.summary_series;
		}

	async wait_on_summary_derived( timeout = 60*1000 ) {

		var count = timeout / busy_loop;
		while( (this._summary_derived == null) && (count > 0)  ) {

			count--;
			await sleep(busy_loop);
			}

		if( count <= 0 )
			return null;
		else
			return this.summary_derived;
		}

	async wait_on_data_meta( timeout = 60*1000 ) {

		var count = timeout / busy_loop;
		while( (this._data_meta == null) && (count > 0)  ) {

			count--;
			await sleep(busy_loop);
			}

		if( count <= 0 )
			return null;
		else
			return this.data_meta;
		}

	async wait_on_data_series( timeout = 60*1000 ) {

		var count = timeout / busy_loop;
		while( (this._data_series == null) && (count > 0)  ) {

			count--;
			await sleep(busy_loop);
			}

		if( count <= 0 )
			return null;
		else
			return this.data_series;
		}

	async wait_on_data_derived( timeout = 60*1000 ) {

		var count = timeout / busy_loop;
		while( (this._data_derived == null) && (count > 0)  ) {

			count--;
			await sleep(busy_loop);
			}

		if( count <= 0 )
			return null;
		else
			return this.data_derived;
		}

	} // Datasets

// create a global dataset for everyone
var dataset = new Datasets(updateURL, data_metaURL, data_seriesURL, summary_metaURL, summary_seriesURL);
dataset.periodic_update( true );
