// some global variables

// partial HTML IDs for specific components
function map_html_target( id = 1 ) { return 'overview_map__' + id; }
function summary_html_target( id = 1 ) { return 'summary__' + id; }
function predict_html_target( id = 1 ) { return 'predict__' + id; }
function preview_html_target( id = 1 ) { return 'preview__' + id; }

// some useful timings, in milliseconds
const busy_loop = 80;
const waiting_for_setup = 1000;

// where to find various JSONs
const updateURL         = "json/status.json";
const data_metaURL      = "json/data_meta.json";
const data_seriesURL    = "json/data_series.json";
const summary_metaURL   = "json/summary_meta.json";
const summary_seriesURL = "json/summary_series.json";

// used to help locate the layout
const layout_element = 'panels';


// https://stackoverflow.com/questions/951021/what-is-the-javascript-version-of-sleep
function sleep(ms = busy_loop) { return new Promise(resolve => setTimeout(resolve, ms)); }


// handy to check if an HTML element is there
async function wait_until_visible( name ) {

	var ele = document.getElementById( name );
	var count = 60*1000 / busy_loop;
	while( (!ele) && (count > 0) ) {

		count--;
		await sleep( busy_loop );
		ele = document.getElementById( name ); 
		}

	if( count <= 0 )
		return null;
	else
		return ele;
	}

// ditto for Javascript obects
async function wait_until_present( thing ) {

	var count = 60*1000 / busy_loop;
	while( (thing == null) && (count > 0)  ) {

		count--;
		await sleep(busy_loop);
		}

	if( count <= 0 )
		return null;
	else
		return thing;
	}

// detect if we're (probably) on a phone
let isMobile = window.matchMedia("only screen and (max-width: 760px)").matches;

