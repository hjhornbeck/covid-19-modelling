// seems like a good colourblind-friendly diverging scheme for the map (thanks, ColorBrewer2!)
const map_colours = ["#8F0353","#920D57","#95155A","#991B5E","#9C2161","#9F2664","#A22B68","#A52F6B","#A8336E","#AB3772","#AF3C75","#B24079","#B5447D","#B84881","#BB4B84","#BE5087","#C1538B","#C4578F","#C85B92","#CA5F96","#CE629A","#D0679D","#D36BA1","#D66EA5","#DA72A9","#DD76AD","#DF7AB0","#DF7DB1","#E080B3","#E283B5","#E287B7","#E38AB8","#E58EBA","#E691BC","#E694BE","#E698C0","#E79BC2","#E99EC3","#E9A2C6","#EAA5C8","#EBA9C9","#EBACCB","#ECAFCD","#EDB2CF","#EEB5D0","#EEB9D3","#EEBCD4","#EFC0D7","#F0C3D9","#F0C7DB","#F1CADC","#F2CDDF","#F2D1E1","#F3D4E2","#F3D7E5","#F4DBE7","#F4DDE8","#F5E1EB","#F5E4EC","#F6E7EE","#F6EBF0","#F7EEF2","#F7F1F4","#F7F4F6","#F7F7F7","#F5F6F3","#F1F5EE","#EEF3E9","#ECF2E4","#E9EFE0","#E5EEDA","#E3EDD6","#E0EBD1","#DDEACD","#DAE9C8","#D7E7C4","#D4E5BF","#D1E4BA","#CDE2B6","#CAE1B1","#C7DFAC","#C4DEA8","#C1DCA3","#BFDB9E","#BBD99A","#B8D795","#B5D690","#B3D48C","#AFD388","#ABD183","#A9D07E","#A6CE7A","#A2CD75","#9FCC70","#9CC96B","#99C867","#95C662","#92C55E","#8FC358","#8BC253","#88C04F","#84BF49","#81BD44","#7EBB40","#7AB73F","#77B43D","#74B03B","#6FAC3A","#6CA938","#69A537","#65A236","#629E34","#5F9B32","#5C9830","#58942F","#55902E","#518D2C","#4E8A2A","#4A8729","#478327","#448026","#417C24","#3D7923","#397521","#367320","#336F1E","#2F6C1D","#2C681C","#29651A"];

// to save recalculating this, build an array of default styles
var lga_styles = new Array();

// start building the map

async function redraw_map( target, state ) {

	// we could use some event handlers
	function highlightFeature(e) {
		var layer = e.target;

		    layer.setStyle({
			            weight: 5,
			            color: '#666',
			            dashArray: '',
			            fillOpacity: 0.7
			        });

		    if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
			            layer.bringToFront();
			        }
		}

	// center somewhere around Alberta
	var map = L.map( target ).setView([55.0136523,-118.9682052], 5);

	// we need this in place
	await dataset.wait_on_data_derived();

	// create an info box
	// source: https://leafletjs.com/examples/choropleth/
	var infoBox = L.control();
	infoBox.onAdd = function(map) {

		this._div = L.DomUtil.create('div', 'lga_info');
		this.update();
		return this._div;
		};

	infoBox.update = function (args) {

		var temp = '';
		if( args ) {
			temp += '<span class="infobox_name">' + 
				dataset.data_meta.names[args] +
				'</span>';
			temp += '<br /><span class="infobox_r" ' + 
				'title="Averaged over the last 14 days">' +
				'Avg daily cases: ' +
				dataset.data_derived.meta.hist_mean[args].toFixed(2) +
				'</span>';
				// ≈ ?</span>'; 
		}
		else
			temp += '<span class="infobox_info">Hover over a local' + 
				' geographic area.<br />&nbsp;</span>';

		this._div.innerHTML = temp;
		};

	infoBox.addTo(map);

	// create a separate pane, so we slide under the labels
	// source: https://leafletjs.com/examples/map-panes/
	map.createPane('text_labels');
	map.getPane(   'text_labels').style.zIndex = 625;
	map.getPane(   'text_labels').style.pointerEvents = 'none';

	const cartoAttrib = '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>' + 
		' contributors, &copy; <a href="https://carto.com/attribution">CARTO</a>';

	// basemap first
	L.tileLayer('https://{s}.basemaps.cartocdn.com/light_nolabels/{z}/{x}/{y}.png', {
	    attribution: cartoAttrib,
	    maxZoom: 18,
	    pane: 'tilePane'
	}).addTo(map);

	// ensure our datasets are in place, and speed up some things
	const max_inv     = 1 / dataset.data_derived.meta.max_mean;
	const colour_scale = (map_colours.length - 1) >> 1;

	// start drawing districts
	for( var lga in dataset.data_meta['bounds'] )

		// iterate down the heirarchy
		for( var geo in dataset.data_meta['bounds'][lga] ) {

			// what colour should we pick?
			var name = dataset.data_meta.names[lga];
			var level = dataset.data_derived.meta.hist_mean[lga];

			var idx = map_colours.length - 1;		// zero = 'cool'
			if( level > 0 )					// non-zero = 'neutral' -> 'warm'
				idx = Math.floor( Math.pow(1 - level*max_inv, 4)*colour_scale + 0.5 );

			var chosen = map_colours[ idx ];

			lga_styles[lga] = { weight: 2, opacity: 1, color: "#ddddff", dashArray: '3',
				fillColor: chosen, fillOpacity: 0.5, pane: 'overlayPane' };

			var bounds = L.polygon( dataset.data_meta['bounds'][lga][geo], lga_styles[lga] );

			// based on https://leafletjs.com/examples/choropleth/
			function build_highlighted(lga) { return function highlighted(e) {

				// console.log( "DEBUG: highlighting " + lga );

				// clone and modify the style
				var new_style = Object.assign( {}, lga_styles[lga] );

				new_style['weight'] = 3;
				new_style['color'] = "#330066";
				new_style['dashArray'] = "";
				new_style['fillOpacity'] = 0.9;

				e.target.setStyle( new_style );

				if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge)
					 e.target.bringToFront();

				infoBox.update(lga);
				} }

			function build_faded(lga) { 

				return function faded(e) {
					e.target.setStyle( lga_styles[lga] ); 
					infoBox.update();
				} }

			function build_click(lga) { 
				return function clicked(e) {
					// add in some extras for touchscreens without hover support
					if (isMobile)
						(build_highlighted(lga))(e);
					previewManager.click(lga);
					if (isMobile) {
						async function dehighlight() {await sleep(750); e.target.setStyle( lga_styles[lga] );};
						dehighlight();
						}
					}
				}

			bounds.on( {mouseover: build_highlighted(lga), mouseout: build_faded(lga), 
				click:build_click(lga)} );

			// finally, add it
			bounds.addTo( map );

			}

	L.tileLayer('https://{s}.basemaps.cartocdn.com/light_only_labels/{z}/{x}/{y}.png', {
	    attribution: cartoAttrib,
	    maxZoom: 18,
	    pane: 'text_labels'
	}).addTo(map);


	}
